/*
 * Codeg DXF to GCODE convertor 
 * Copyright (C) 2006 Eric Poulsen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 *
 * Author contact info:
 *
 * Eric Poulsen
 * email: eric@zyxod.com
 *   AIM: epoulsen2
 *   ICQ: 1608780
 *
 */


#include "dxf.hpp"
#include <iostream>
#include <sstream>
#include <string>
#include <math.h>
using namespace std;

#define PI ((dxfFloat)3.141592653589793238462643383279)
#define PI2 ((dxfFloat)3.141592653589793238462643383279 * (dxfFloat)2)
#define PIHALF ((dxfFloat)3.141592653589793238462643383279 / (dxfFloat)2)

ostream & operator << (ostream & os, const dxfFloat & x)
{
	dxfFloat y = x.round(12);
	char * q = y.toFixPtStringExp(-1,'.','\0',3);
	os << q;
	free(q);
	return os;
}

double dxfFloat2double(const dxfFloat x)
{
	char * q = x.toFixPtStringExp(20,'.','\0',3);
	double y = atof(q);
	free(q);
	return y;
}

/* translateAttr */

translateAttr::translateAttr(void) : 
	xy_angle(0),
	yz_angle(0),
	xz_angle(0),
	x_enable(true),
	y_enable(true),
	z_enable(false),
	max_line_seg(0.01)
{
}

/* dxfObject */

dxfObject::dxfObject(void) : reversed(false)
{
}
dxfObject::~dxfObject(void)
{
}

void dxfObject::dump(const char * prefix, ostream & out) const
{
	out << prefix << "dxfObject::dump() stub" << endl;
}

void dxfObject::push_back(dxfObjectPtr p)
{
	cerr << "dxfObject::push_back(dxfObjectPtr p) called for object of type '" << typeid(*this).name() << "' THIS IS AN ERROR. " << __FILE__ << ":" << __LINE__;
}

string dxfObject::pretty_coord (const dxfPoint & p)
{
	return pretty_coord(p.x,p.y,p.z);
}

string dxfObject::pretty_coord (dxfFloat x, dxfFloat y, dxfFloat z)
{
	ostringstream ret;
	//ret.precision(20);
	ret << "["<< x << ", " << y << ", " << z << "]";
	return ret.str();
}

bool dxfObject::isContainer(void)
{
	return false;
}

bool dxfObject::isDrawable(void)
{
	return false;
}

dxfPoint dxfObject::beginPoint(void) const
{
	cerr << "dxfObject::begin(void) called -- THIS IS AN ERROR -- this virtual function is not implemented in the derived class you're calling" << endl;
	return dxfPoint();
}

dxfPoint dxfObject::endPoint(void) const
{
	cerr << "dxfObject::end(void) called -- THIS IS AN ERROR -- this virtual function is not implemented in the derived class you're calling" << endl;
	return dxfPoint();
}

bool dxfObject::reverse(void)
{
	return (reversed = !reversed);
}

dxfObjectPtrVector::iterator dxfObject::begin(void)
{
	return (dxfObjectPtrVector::iterator)NULL;
}

dxfObjectPtrVector::iterator dxfObject::end(void)
{
	return (dxfObjectPtrVector::iterator)NULL;
}

void dxfObject::toGcode(ostream & out, int & line_num, const translateAttr & t, bool output_first_point) const
{
	cerr << "dxfObject::toGcode() stub called.  This is probably an error." << endl;
}

void dxfObject::toPoints(dxfPointVector & out, const translateAttr & t, bool output_first_point) const
{
	cerr << "dxfObject::toGcode() stub called.  This is probably an error." << endl;
}

dxfFloat dxfObject::mod(dxfFloat x, dxfFloat m)
{
	dxfFloat z = (x/m).floor();
	return x - (z*m);

}
dxfPoint dxfObject::calcVector(dxfPoint center, dxfFloat radius, dxfFloat angle)
{
	dxfFloat a = deg2rad(angle);
	return dxfPoint(center.x + radius * cos(a), center.y + radius * sin(a),center.z);
}

dxfFloat dxfObject::deg2rad(dxfFloat d) 
{
	return d % 360 / 360 * PI2;
}

dxfFloat dxfObject::rad2deg(dxfFloat r) 
{
	return r% PI2 / PI2 * 360;
}

dxfFloat dxfObject::distance2D(const dxfPoint & p1, const dxfPoint & p2) 
{
	dxfFloat xdiff = p1.x - p2.x;
	dxfFloat ydiff = p1.y - p2.y;
	return sqrt(xdiff * xdiff + ydiff*ydiff);
}

dxfFloat dxfObject::abs(dxfFloat q)
{
	return q >= 0 ? q : -q;
}

dxfFloat dxfObject::calcAngle(const dxfPoint & from, const dxfPoint & to)
{
	//static int quadrature[] = {0,-180,0,180};
	static int quadrature[] = {0,90,270,180};
	static int flip[] = {0,90,90,0};
	dxfFloat ydiff = to.y - from.y;
	dxfFloat xdiff = to.x - from.x;

	int xquad = xdiff < 0 ? 1 : 0; 
	int yquad = ydiff < 0 ? 2 : 0;
	// 0 = +x, +y == Q1 0-90
	// 1 = -x, +y == Q2 90-180
	// 2 = +x, -y == Q4 270-360
	// 3 = -x, -y == Q3 180-270
	
	int q = xquad+yquad;

	if (xdiff == 0 && ydiff == 0)
		return 0;
	if (xdiff == 0) // avoid divide by zero
		return ydiff >= 0 ? 90 : 270;

	xdiff = xdiff.abs();
	ydiff = ydiff.abs();
	dxfFloat ratio = (ydiff / xdiff).round(10); // << HUGE (4X faster) performance increase by rounding here.
	dxfFloat angle = rad2deg(ratio.atan());
	angle = (angle - flip[q]).abs() + quadrature[q];
	//cout << " quad angle: " << quad_angle << endl;

 return angle;

}
dxfFloat dxfObject::angleDiff(const dxfFloat a1, const dxfFloat a2)
{
	dxfFloat z = (a1-a2).abs() % 360;
	return z > 180 ? 360 - z : z;
}

dxfPoint dxfObject::rotatePoint2D(const dxfPoint & center, const dxfPoint & p, dxfFloat angle)
{
	dxfFloat radius = abs(distance2D(center,p));
	dxfFloat cur_angle = calcAngle(center,p);
	return calcVector(center,radius,cur_angle+angle);
}

dxfFloat dxfObject::min(dxfFloat x, dxfFloat y)
{
	return x > y ? y : x;
}
dxfFloat dxfObject::max(dxfFloat x, dxfFloat y)
{
	return x < y ? y : x;
}

void dxfObject::printGcodeLine(ostream & out,int & line_num, int inc)
{
	char old_fill = out.fill();
	streamsize old_width = out.width();

	out << "N";

	out.fill('0');
	out.width(5);
	out << right << line_num << " ";
	out.fill(old_fill);
	out.width(old_width);
	line_num += inc;
}

void dxfObject::translate(const translateAttr & t)
{
	cerr << "dxfObject::translate() called.  This is probably an error." << endl;
}

dxfPoint dxfObject::translatePoint(const dxfPoint & p, const translateAttr & t)
{
	dxfPoint r = p;
	dxfPoint temp;

	if (mod(t.xy_angle,360) != 0)
		r = rotatePoint2D(t.xy_center,r,t.xy_angle);

	if (mod(t.yz_angle,360) != 0)
	{
		temp.x = r.y;
		temp.y = r.z;
		rotatePoint2D(t.yz_center,temp,t.yz_angle);
		r.y = temp.x;
		r.z = temp.y;
	}

	if (mod(t.xz_angle,360) != 0)
	{
		temp.x = r.x;
		temp.y = r.z;
		rotatePoint2D(t.xz_center,temp,t.xz_angle);
		r.x = temp.x;
		r.z = temp.y;
	}
	r.x += t.offset.x;
	r.y += t.offset.y;
	r.z += t.offset.z;

	return r;

}

void dxfObject::gcodePoint(ostream & out, const dxfPoint & p, const translateAttr & t)
{
	dxfPoint q = translatePoint(p,t);
	bool output = false;
	if (t.x_enable)
	{
		out << "X" << q.x;
		output = true;
	}

	if (t.y_enable)
	{
		out << (output ? " " : "") << "Y" << q.y;
		output = true;
	}

	if (t.z_enable)
		out << (output ? " " : "") << "Z" << q.z;
}


/* Doc */

dxfDoc::dxfDoc(void)
{
}

dxfDoc::~dxfDoc(void)
{
}

void dxfDoc::dump(const char * prefix, ostream & out) const
{
	string p = prefix;
	p += " ";
	out << prefix << "dxfDoc:" << endl;
	for(dxfObjectPtrVector::const_iterator i = objects.begin(); i != objects.end(); i++)
		(**i).dump(p.c_str(), out);
}
void dxfDoc::translate(const translateAttr & t)
{
	for(dxfObjectPtrVector::const_iterator i = objects.begin(); i != objects.end(); i++)
		(**i).translate(t);
}

void dxfDoc::push_back(dxfObjectPtr p) { objects.push_back(p); }

bool dxfDoc::isContainer(void)
{
	return true;
}

dxfObjectPtrVector::iterator dxfDoc::begin(void)
{
	return objects.begin();
}

dxfObjectPtrVector::iterator dxfDoc::end(void)
{
	return objects.end();
}

/* Layer */

dxfLayer::dxfLayer(const string & name, int flags) : name(name), flags(flags),visible(! (flags & 0x01)) {}
dxfLayer::~dxfLayer(void) {}

void dxfLayer::dump(const char * prefix, ostream & out) const
{ 
	out << prefix << "Layer: '" << name << "' flags: 0x" << hex << flags << dec << endl; 
	string p = prefix;
	p += " ";
	for(dxfObjectPtrVector::const_iterator i = objects.begin(); i != objects.end(); i++)
		(**i).dump(p.c_str(), out);
}
void dxfLayer::push_back(dxfObjectPtr p) { objects.push_back(p); }
void dxfLayer::translate(const translateAttr & t)
{
	for(dxfObjectPtrVector::const_iterator i = objects.begin(); i != objects.end(); i++)
		(**i).translate(t);
}

bool dxfLayer::isContainer(void)
{
	return true;
}

dxfObjectPtrVector::iterator dxfLayer::begin(void)
{
	return objects.begin();
}

dxfObjectPtrVector::iterator dxfLayer::end(void)
{
	return objects.end();
}

/* Block */

dxfBlock::dxfBlock(const string & name, int flags, dxfFloat x, dxfFloat y, dxfFloat z) : name(name), flags(flags), x(x), y(y), z(z) {}
dxfBlock::~dxfBlock(void) {}

void dxfBlock::dump(const char * prefix, ostream & out) const
{
	out << prefix << "Block: '" << name << "' flags: 0x" << hex << dec << flags << " "
		<< pretty_coord(x,y,z) <<  endl;
	string p = prefix;
	p += " ";
	for(dxfObjectPtrVector::const_iterator i = objects.begin(); i != objects.end(); i++)
		(**i).dump(p.c_str(), out);
}

void dxfBlock::push_back(dxfObjectPtr p) { objects.push_back(p); }

bool dxfBlock::isContainer(void)
{
	return true;
}

dxfObjectPtrVector::iterator dxfBlock::begin(void)
{
	return objects.begin();
}

dxfObjectPtrVector::iterator dxfBlock::end(void)
{
	return objects.end();
}

void dxfBlock::translate(const translateAttr & t)
{
	for(dxfObjectPtrVector::const_iterator i = objects.begin(); i != objects.end(); i++)
		(**i).translate(t);
}

/* Point */


dxfPoint::dxfPoint(dxfFloat xx, dxfFloat yy, dxfFloat zz) : x(xx), y(yy), z(zz) 
{
	round();
}

dxfPoint::dxfPoint(void) : x(0),y(0),z(0) 
{
}

void dxfPoint::translate(const translateAttr & t)
{
	*this = translatePoint(*this,t);
	round();
}

bool dxfPoint::operator == (const dxfPoint & p)
{
	dxfFloat x_diff = abs(this->x-p.x);
	dxfFloat y_diff = abs(this->y-p.y);
	dxfFloat z_diff = abs(this->z-p.z);
	return (x_diff < DXFPOINT_EQUAL_THRESHOLD) && (y_diff < DXFPOINT_EQUAL_THRESHOLD) && (z_diff < DXFPOINT_EQUAL_THRESHOLD);
}

dxfPoint::~dxfPoint(void) {}

void dxfPoint::dump(const char * prefix, ostream & out) const
{
	out << prefix << "Point: " << pretty_coord(x,y,z) <<  endl;
}

bool dxfPoint::isDrawable(void) { return true; }
void dxfPoint::toGcode(ostream & out, int & line_num, const translateAttr & t, bool output_first_point) const
{
	if (output_first_point)
	{
		printGcodeLine(out, line_num);
		out << "G01 ";
		gcodePoint(out,dxfPoint(x,y,z),t);
		out << endl;
	}
	// Do nothing, intentionally
}
void dxfPoint::toPoints(dxfPointVector & out, const translateAttr & t, bool output_first_point) const
{
	dxfPoint q = translatePoint(dxfPoint(x,y,z),t);
	out.push_back(q);
	/*
	if (output_first_point)
	{
		dxfPoint q = translatePoint(dxfPoint(x,y,z),t);
		out.push_back(q);
	}
	*/
}
void dxfPoint::round(int places)
{
	return;
	dxfFloat m = dxfFloat(10).pow(places);

	x = (x*m).floor() / m;
	y = (y*m).floor() / m;
	z = (z*m).floor() / m;

}

dxfPoint dxfPoint::beginPoint(void) const
{
	return *this;
}

dxfPoint dxfPoint::endPoint(void) const
{
	return *this;
}

/* Line */

dxfLine::dxfLine() : p1(0,0,0), p2(0,0,0) {}
dxfLine::dxfLine(dxfFloat x1, dxfFloat y1, dxfFloat z1, dxfFloat x2, dxfFloat y2, dxfFloat z2) : p1(x1,y1,z1), p2(x2,y2,z2) {}
dxfLine::dxfLine(const dxfPoint & point1,const dxfPoint & point2) : p1(point1), p2(point2) {}
dxfLine::~dxfLine(void) {}

void dxfLine::dump(const char * prefix, ostream & out) const
{
	if (reversed)
		out << prefix << "Line: " << pretty_coord(p2) << " to " << pretty_coord(p1) <<  endl;
	else
		out << prefix << "Line: " << pretty_coord(p1) << " to " << pretty_coord(p2) <<  endl;
}

void dxfLine::translate(const translateAttr & t)
{
	p1.translate(t);
	p2.translate(t);
}
bool dxfLine::isDrawable(void) { return true; }
void dxfLine::toPoints(dxfPointVector & out,  const translateAttr & t, bool output_first_point) const
{
	dxfPoint p1_trans = translatePoint(p1,t);
	dxfPoint p2_trans = translatePoint(p2,t);
	if (reversed)
	{
		if (output_first_point)
			out.push_back(p2_trans);
		out.push_back(p1_trans);
	}
	else
	{
		if (output_first_point)
			out.push_back(p1_trans);
		out.push_back(p2_trans);
	}
}

dxfPoint dxfLine::nearestPoint(const dxfPoint & p)
{
	dxfFloat line_length = distance2D(p1,p2);

	if (line_length == 0)
		return p1;

	dxfFloat line_len;


	line_len = distance2D( p1, p2 );

	/*
	U = ( ( ( Point->X - LineStart->X ) * ( LineEnd->X - LineStart->X ) ) +
			( ( Point->Y - LineStart->Y ) * ( LineEnd->Y - LineStart->Y ) ) +
			( ( Point->Z - LineStart->Z ) * ( LineEnd->Z - LineStart->Z ) ) ) /
		( LineMag * LineMag );
		*/

	dxfFloat u = ((p.x - p1.x)*(p2.x - p1.x) + (p.y - p1.y) * (p2.y - p1.y)) /	( line_len * line_len );

	//cout << pretty_coord(p1) << " to " << pretty_coord(p2) << " u: " << u << endl;

	if (u < 0)
		return p1;

	if (u > 1)
		return p2;


	return dxfPoint(p1.x+u*(p2.x-p1.x),p1.y+u*(p2.y-p1.y),0);

}

/*
dxfPoint dxfLine::nearestPoint(const dxfPoint & p)
{
	dxfFloat line_length = distance2D(p1,p2);

	if (line_length == 0)
		return p1;

	dxfFloat line_angle = calcAngle(p1,p2); // Angle of line itself, p1 as reference
	

	dxfFloat a1 = calcAngle(p1,p);

	if (angleDiff(line_angle,a1) > 90)
		return p1;

	dxfFloat a2 = calcAngle(p2,p);
	if (angleDiff((line_angle +180) % 360,a2)> 90)
		return p2;

	dxfFloat hypotenuse = distance2D(p,p1);
	dxfFloat angle = 90 - ((a1-line_angle).abs() % 90);
	dxfFloat sine = deg2rad(angle).sin();
	dxfFloat opposite = sine * hypotenuse;
	dxfFloat distance_ratio = opposite / line_length;
	dxfFloat xdiff = p2.x - p1.x;
	dxfFloat ydiff = p2.y - p1.y;

	return dxfPoint(p1.x + xdiff * distance_ratio, p1.y + ydiff * distance_ratio, 0);
}
*/

void dxfLine::toGcode(ostream & out, int & line_num, const translateAttr & t, bool output_first_point) const
{
	printGcodeLine(out,line_num);
	out << "(dxfLine" <<(reversed ? " reversed" : "") << ")" << endl;
	printGcodeLine(out, line_num);
	dxfPoint p1_trans = translatePoint(p1,t);
	dxfPoint p2_trans = translatePoint(p2,t);
	if (reversed)
	{
		if (output_first_point)
		{
			out << "G01 "; gcodePoint(out,p2_trans,t);	out << endl;
			printGcodeLine(out, line_num);
		}
		out << "G01 "; gcodePoint(out,p1_trans,t);	out << endl;
	}
	else
	{
		if (output_first_point)
		{
			out << "G01 "; gcodePoint(out,p1_trans,t);	out << endl;
			printGcodeLine(out, line_num);
		}
		out << "G01 "; gcodePoint(out,p2_trans,t);	out << endl;
	}
}

dxfPoint dxfLine::beginPoint(void) const
{
	return reversed ? p2 : p1;
}

dxfPoint dxfLine::endPoint(void) const
{
	return reversed ? p1 : p2;
}

/* Arc */ 

dxfArc::dxfArc(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat radius, dxfFloat angle1, dxfFloat angle2) : 
	center(x,y,z), 
	radius(radius), 
	angle1(angle1), 
	angle2(angle2),
	end_point_1(calcVector(center,radius,angle1)),
	end_point_2(calcVector(center,radius,angle2))
{
}

dxfArc::~dxfArc(void) {}

void dxfArc::dump(const char * prefix, ostream & out) const
{
	out << prefix ;
	out << "Arc: " << pretty_coord(center) << " radius: " << radius << " angle: " << angle1 << " to " << angle2 << " " << pretty_coord(end_point_1) << " to " << pretty_coord(end_point_2) << endl;
}

void dxfArc::translate(const translateAttr & t)
{
	center.translate(t);
	end_point_1 = calcVector(center,radius,angle1);
	end_point_2 = calcVector(center,radius,angle2);
}

bool dxfArc::isDrawable(void) { return true; }
void dxfArc::toPoints(dxfPointVector & out,  const translateAttr & t, bool output_first_point) const
{
	dxfPoint center_trans = translatePoint(center,t);
	dxfFloat arc_width = abs(angle1-angle2);
	dxfFloat circumference = radius * PI2;
	dxfFloat arc_length = (arc_width / 360) * circumference;
	dxfFloat step = arc_width / (arc_length / t.max_line_seg);
	if (step > arc_width)
		step = arc_width / 2;
	bool last = false;
	bool first = true;
	/*
		 cout << "dxfArc::toPoints" << endl;
		 cout << " center_trans: " << pretty_coord(center_trans) << endl;
		 cout << " arc_width: " << arc_width << endl;
		 cout << " circumference: " << circumference << endl;
		 cout << " arc_length: " << arc_length << endl;
		 cout << " step: " << step << endl;
		 cout << " max_line_seg: " << t.max_line_seg << endl;
		 */

	if (reversed)
	{
		dxfFloat start_angle = angle2;
		dxfFloat end_angle = angle1;
		if (end_angle > start_angle)
			end_angle -= 360;

		for(dxfFloat i = start_angle; !last ; i -= step)
		{
			if (!output_first_point && first)
			{
				first = false;
				continue;
			}
			if (i <= end_angle)
			{
				i = end_angle;
				last = true;
			}
			out.push_back(calcVector(center_trans,radius,i));
		}
	}
	else
	{
		dxfFloat start_angle = angle1;
		dxfFloat end_angle = angle2;
		if (end_angle < start_angle)
			end_angle += 360;

		for(dxfFloat i = start_angle; !last ; i += step)
		{
			if (!output_first_point && first)
			{
				first = false;
				continue;
			}
			if (i >= end_angle)
			{
				i = end_angle;
				last = true;
			}
			out.push_back(calcVector(center_trans,radius,i));
		}
	}


}

void dxfArc::toGcode(ostream & out, int & line_num, const translateAttr & t, bool output_first_point) const
{
	printGcodeLine(out,line_num);
	out << "(dxfArc" <<(reversed ? " reversed" : "") << ")" << endl;
	dxfPoint begin_trans = translatePoint(beginPoint(),t);
	dxfPoint end_trans = translatePoint(endPoint(),t);
	dxfPoint center_trans = translatePoint(center,t);

	// Offsets from start point to center
	dxfFloat x_off = begin_trans.x - center_trans.x;
	dxfFloat y_off = begin_trans.y - center_trans.y;

	printGcodeLine(out, line_num);
	if (output_first_point)
	{
		out << "G01 "; gcodePoint(out,begin_trans,t);	out << endl;
		printGcodeLine(out, line_num);
	}
	out << (reversed ? "G02 " : "G03 ");
	gcodePoint(out,end_trans,t);
	out << " I" << -x_off << " J" << -y_off << endl;
}

dxfPoint dxfArc::beginPoint(void) const
{
	return reversed ? end_point_2 : end_point_1;
}

dxfPoint dxfArc::endPoint(void) const
{
	return reversed ? end_point_1 : end_point_2;
}

/* Circle */

dxfCircle::dxfCircle(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat radius) : center(x,y,z), radius(radius) {}
dxfCircle::~dxfCircle(void) {}

void dxfCircle::dump(const char * prefix, ostream & out) const
{
	out << prefix << "Circle: " << pretty_coord(center) << " radius: " << radius << endl;
}

void dxfCircle::translate(const translateAttr & t)
{
	center.translate(t);
}
bool dxfCircle::isDrawable(void) { return true; }
dxfPoint dxfCircle::beginPoint(void) const
{
	return dxfPoint(center.x+radius,center.y,center.z);
}
dxfPoint dxfCircle::endPoint(void) const
{

	return dxfPoint(center.x+radius,center.y,center.z);
}

void dxfCircle::toPoints(dxfPointVector & out,  const translateAttr & t, bool output_first_point) const
{
	dxfPoint center_trans = translatePoint(center,t);
	dxfFloat circumference = radius * PI2;
	dxfFloat step = 360.0 / (circumference / t.max_line_seg);
	bool last = false;
	bool first = true;


	if (reversed)
	{
		for(dxfFloat i = 360; !last ; i -= step)
		{
			if (!output_first_point && first)
			{
				first = false;
				continue;
			}
			if (i <= 0)
			{
				i = 0;
				last = true;
			}
			out.push_back(calcVector(center_trans,radius,i));
		}
	}
	else
	{
		for(dxfFloat i = 0; !last ; i += step)
		{
			if (!output_first_point && first)
			{
				first = false;
				continue;
			}
			if (i >= 360)
			{
				i = 360;
				last = true;
			}
			out.push_back(calcVector(center_trans,radius,i));
		}
	}
}

void dxfCircle::toGcode(ostream & out, int & line_num, const translateAttr & t, bool output_first_point) const
{
	printGcodeLine(out,line_num);
	out << "(dxfCircle" <<(reversed ? " reversed" : "") << ")" << endl;
	dxfPoint center_trans = translatePoint(center,t);
	dxfPoint begin_trans = center_trans;
	begin_trans.x+= radius;

	// Offsets from start point to center
	dxfFloat x_off = -radius;
	dxfFloat y_off = 0;

	printGcodeLine(out, line_num);
	if (output_first_point)
	{
		out << "G01 "; gcodePoint(out,begin_trans,t);	out << endl;
		printGcodeLine(out, line_num);
	}
	out << (reversed ? "G02 " : "G03 ");
	gcodePoint(out,begin_trans,t);
	out << " I" << x_off << " J" << y_off << endl;
}

/* Ellipse */

dxfEllipse::dxfEllipse(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat mx, dxfFloat my, dxfFloat mz, dxfFloat ratio, dxfFloat angle1, dxfFloat angle2): 
	center(x,y,z), 
	major_endpoint(x+mx,y+my,z+mz), 
	ratio(ratio), 
	angle1(angle1), 
	angle2(angle2),
	major_radius(abs(distance2D(center,major_endpoint))),
	minor_radius(major_radius * ratio),
	tilt_angle(calcAngle(center,major_endpoint)),
	end_point_1(plotPoint(angle1)),
	end_point_2(plotPoint(angle2))
{
}

dxfPoint dxfEllipse::plotPoint(dxfFloat angle) const 
{
	dxfFloat x = center.x + major_radius * cos(deg2rad(angle));
	dxfFloat y = center.y + minor_radius * sin(deg2rad(angle));
	dxfPoint p (x,y,0);
	dxfPoint q = rotatePoint2D(center,p,tilt_angle);
	return q;
	//return rotatePoint2D(center,dxfPoint(x,y,center.z),tilt_angle);
}

dxfEllipse::~dxfEllipse(void) {}
void dxfEllipse::translate(const translateAttr & t)
{
	center.translate(t);
	major_endpoint.translate(t);
	end_point_1 = plotPoint(angle1);
	end_point_2 = plotPoint(angle2);

}

void dxfEllipse::dump(const char * prefix, ostream & out) const
{
	out << prefix << "Ellipse: Center: " << pretty_coord(center) 
		<< " Major end point:  " << pretty_coord(major_endpoint) << " ratio: " << ratio 
		<< " angle: " << angle1 << " to " << angle2 << " major radius: " << major_radius 
		<< " minor_radius: " << minor_radius << " tilt angle: " << tilt_angle 
		<< " Begin Point: " << pretty_coord(end_point_1) << " End Point: " << pretty_coord(end_point_2)
		<< endl;
}

bool dxfEllipse::isDrawable(void) { return true; }

dxfPoint dxfEllipse::beginPoint(void) const
{
	return reversed ? end_point_2 : end_point_1;
}

dxfPoint dxfEllipse::endPoint(void) const
{
	return reversed ? end_point_1 : end_point_2;
}

void dxfEllipse::toPoints(dxfPointVector & out, const translateAttr & t, bool output_first_point) const
{
	dxfPoint center_trans = translatePoint(center,t);
	dxfFloat arc_width = abs(angle1-angle2);
	dxfFloat circumference = major_radius * PI2;
	dxfFloat arc_length = (arc_width / 360) * circumference;
	dxfFloat step = arc_width / (arc_length / t.max_line_seg);
	if (step > arc_width)
		step = arc_width / 2;
//	cout << " arc_length: " << arc_length <<  " arc width: " << arc_width << " max line seg: " << t.max_line_seg << " step: " << step << endl;
	bool last = false;
	bool first = true;
	//cout << " ellipse:: to points"<<endl;

	if (reversed)
	{
		dxfFloat start_angle = angle2;
		dxfFloat end_angle = angle1;
		if (end_angle > start_angle)
			end_angle -= 360;

		for(dxfFloat i = start_angle; !last ; i -= step)
		{
			//cout << " angle: " << i << endl;
			if (!output_first_point && first)
			{
				first = false;
				continue;
			}
			if (i <= end_angle)
			{
				i = end_angle;
				last = true;
			}
			out.push_back(plotPoint(i));
		}
	}
	else
	{
		dxfFloat start_angle = angle1;
		dxfFloat end_angle = angle2;
		if (end_angle < start_angle)
			end_angle += 360;

		for(dxfFloat i = start_angle; !last ; i += step)
		{
			//cout << " angle: " << i << endl;
			//cout << " ellipse angle: " << i << endl;
			if (!output_first_point && first)
			{
				first = false;
				continue;
			}
			if (i >= end_angle)
			{
				i = end_angle;
				last = true;
			}
			out.push_back(plotPoint(i));
		}
	}
}
void dxfEllipse::toGcode(ostream & out, int & line_num, const translateAttr & t, bool output_first_point) const
{
	printGcodeLine(out,line_num);
	out << "(dxfEllipse" <<(reversed ? " reversed" : "") << ")" << endl;
	dxfPointVector points;
	// toPoints handles both 'reversed' and 'output_first_point'
	toPoints(points,t,output_first_point);
	for(dxfPointVector::const_iterator i = points.begin(); i != points.end(); i++)
	{
		printGcodeLine(out, line_num);
		out << "G01 ";
		gcodePoint(out,*i,t);
		out << endl;
	}
}

/* Polyline */

dxfPolyline::dxfPolyline(int flags) : flags(flags) {}
dxfPolyline::~dxfPolyline(void) {}

void dxfPolyline::addVertex(const dxfPolylineVertex & v) { vertices.push_back(v); }

void dxfPolyline::dump(const char * prefix, ostream & out) const
{
	out << prefix << "Polyline: Flags: 0x" << hex << flags << dec << endl;
	out << prefix << " Vertices:" << endl;
	string p(prefix);
	p += "  ";
	for(dxfPolylineVertexVector::const_iterator i = vertices.begin(); i != vertices.end(); i++)
		i->dump(p.c_str(),out);
}

void dxfPolyline::translate(const translateAttr & t)
{
	for(dxfPolylineVertexVector::iterator i = vertices.begin(); i != vertices.end(); i++)
		i->translate(t);
}

bool dxfPolyline::isDrawable(void) { return !vertices.empty(); }
void dxfPolyline::toPoints(dxfPointVector & out, const translateAttr & t, bool output_first_point) const
{
	bool first = true;
	if (reversed)
		// Can't use const_reverse_iterator -- bug in GCC/STL
		//for(dxfPolylineVertexVector::const_reverse_iterator i = vertices.rbegin(); i != vertices.rend(); i++)
		for(dxfPolylineVertexVector::const_reverse_iterator i = vertices.rbegin(); i != vertices.rend(); i++)
		{
			if (first && !output_first_point)
			{
				first = false;
				continue;
			}
			out.push_back(translatePoint(i->p,t));
		}
	else
		for(dxfPolylineVertexVector::const_iterator i = vertices.begin(); i != vertices.end(); i++)
		{
			if (first && !output_first_point)
			{
				first = false;
				continue;
			}
			out.push_back(translatePoint(i->p,t));
		}
}

void dxfPolyline::toGcode(ostream & out, int & line_num, const translateAttr & t, bool output_first_point) const
{
	printGcodeLine(out,line_num);
	out << "(dxfPolyline" <<(reversed ? " reversed" : "") << ")" << endl;
	dxfPointVector points;
	// toPoints handles both 'reversed' and 'output_first_point'
	toPoints(points,t,output_first_point); 
	for(dxfPointVector::const_iterator i = points.begin(); i != points.end(); i++)
	{
		printGcodeLine(out, line_num);
		out << "G01 ";
		gcodePoint(out,*i,t);
		out << endl;
	}
}

dxfPolylineVertex::dxfPolylineVertex(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat bulge) : p(x,y,z), bulge(bulge) {}
dxfPolylineVertex::~dxfPolylineVertex() {}

void dxfPolylineVertex::dump(const char * prefix,ostream & out) const
{
	out << prefix << " " << pretty_coord(p) << " Bulge: " << bulge << endl;
}

dxfPoint dxfPolyline::beginPoint(void) const
{
	if (vertices.empty())
		return dxfPoint();
	else
		return reversed ? vertices.rbegin()->p : vertices.begin()->p;
}

dxfPoint dxfPolyline::endPoint(void) const
{
	if (vertices.empty())
		return dxfPoint();
	else
		return reversed ? vertices.begin()->p : vertices.rbegin()->p;
}

void dxfPolylineVertex::translate(const translateAttr & t)
{
	p.translate(t);
}

/* Spline */

dxfSpline::dxfSpline(int flags, int degree, bool closed) : flags(flags), degree(degree), closed(closed) {}
dxfSpline::~dxfSpline(void) 
{
}
void dxfSpline::addKnot (dxfFloat k) { knots.push_back(k);}
void dxfSpline::addPoint (const dxfPoint & p) { points.push_back(p); }
void dxfSpline::dump(const char * prefix,ostream & out) const
{
	out << prefix << "Spline: flags: 0x" << hex << flags << dec << " degree: " << degree << " closed: " << (closed ? "Yes" : "No" ) << endl;
	string p(prefix);
	p += "  ";
	out << prefix << " Points:" << endl;
	for(dxfPointVector::const_iterator i = points.begin(); i != points.end(); i++)
		i->dump(p.c_str(),out);
	out << prefix << " Knots:" << endl;
	for(dxfFloatVector::const_iterator i = knots.begin(); i != knots.end(); i++)
		out << p << *i << endl;
}
void dxfSpline::translate(const translateAttr & t)
{
	for(dxfPointVector::iterator i = points.begin(); i != points.end(); i++)
		i->translate(t);
}

bool dxfSpline::isDrawable(void) { return !points.empty(); }
void dxfSpline::toGcode(ostream & out, int & line_num, const translateAttr & t, bool output_first_point) const
{
	printGcodeLine(out,line_num);
	out << "(dxfSpline" <<(reversed ? " reversed" : "") << ")" << endl;
	dxfPointVector points;
	// toPoints handles both 'reversed' and 'output_first_point'
	toPoints(points,t,output_first_point); 
	for(dxfPointVector::const_iterator i = points.begin(); i != points.end(); i++)
	{
		printGcodeLine(out, line_num);
		out << "G01 ";
		gcodePoint(out,*i,t);
		out << endl;
	}
}

dxfPoint dxfSpline::beginPoint(void) const
{
	if (points.empty())
		return dxfPoint();
	else
		return reversed ? *(points.rbegin()) : *(points.begin());
}

dxfPoint dxfSpline::endPoint(void) const
{
	if (points.empty())
		return dxfPoint();
	else
		return reversed ? *(points.begin()) : *(points.rbegin());
}

void dxfSpline::toPoints(dxfPointVector & out, const translateAttr & t, bool output_first_point) const
{
	int grain = 1;

	dxfPointVector cp = points;

	if (closed)
		for (int i=0; i < degree; ++i) 
			cp.push_back(points[i]);

DXFSPLINE_PLOT_RESTART:
	int i;
	int npts = cp.size();
	// order:
	int k = degree+1;
	// resolution:
	int p1 = grain * npts;
	//cout << " Points to generate: " << p1 << endl;

	double* b = new double[npts*3+1];
	double* h = new double[npts+1];
	double* p = new double[p1*3+1];

	i = 0;
	for (dxfPointVector::iterator it = cp.begin(); it!=cp.end(); it++) 
	{
		b[i++] = dxfFloat2double((*it).x);
		b[i++] = dxfFloat2double((*it).y);
		b[i++] = 0.0;
	}

	// set all homogeneous weighting factors to 1.0
	for (i=1; i <= npts; i++) 
		h[i] = 1.0;

	for (i = 1; i <= 3*p1; i++) 
		p[i] = 0.0;

	/*
		 for (int i = 0; i < npts;i++)
		 cout << "cp: " << pretty_coord(b[i*3],b[i*3+1],b[i*3+2]) << endl;
		 */


	if (closed) 
		rbsplinu(npts,k,p1,b,h,p);
	else 
		rbspline(npts,k,p1,b,h,p);

	dxfFloat max_dist = 0;
	dxfPoint last_point(p[0],p[1],p[2]);
	for (i = 1; i < p1; i++) // i = ONE is intentional
	{
		dxfPoint this_point(p[i*3], p[i*3+1], p[i*3+2]);
		max_dist = max(max_dist,distance2D(last_point,this_point));
		last_point = this_point;
	}


	//cout << "peak seg len: " << max_dist << " max allowed: " << t.max_line_seg << endl;

	if (max_dist > dxfFloat2double(t.max_line_seg))
	{ 
		//cout << "Adjusting grain from " << grain << " to ";
		grain = (int)dxfFloat2double((grain * (max_dist / t.max_line_seg) * 1.05).ceil());
		//cout << grain <<", restarting plot" << endl;
		delete[] b;
		delete[] h;
		delete[] p;

		goto DXFSPLINE_PLOT_RESTART;
	}

	bool first = true;
	for (i = 0; i < p1; i++) 
	{
		if (first && !output_first_point)
		{
			first = false;
			continue;
		}
		out.push_back(dxfPoint(p[i*3], p[i*3+1], p[i*3+2]));
		//cout << "p: " << pretty_coord(plotted_points.back()) << endl;
	}
	delete[] b;
	delete[] h;
	delete[] p;
}

// npts = number of control points
// k = degree + 1
// p1 =  # of plotted points
// b = control points
// h = Weighting factors
// p = output points

void dxfSpline::rbspline(int npts, int k, int p1,	double b[], double h[], double p[]) const
{

	int i,j,icount,jcount;
	int i1;
	int nplusc;

	double step;
	double t;
	double temp;

	nplusc = npts + k;

	int* x = new int[nplusc+1];
	double* nbasis = new double[npts+1];

	// zero and redimension the knot vector and the basis array

	for(i = 0; i <= npts; i++) {
		nbasis[i] = 0.0;
	}

	for(i = 0; i <= nplusc; i++) {
		x[i] = 0;
	}

	// generate the uniform open knot vector
	knot(npts,k,x);

	icount = 0;

	// calculate the points on the rational B-spline curve
	t = 0;
	step = ((double)x[nplusc])/((double)(p1-1));

	for (i1 = 0; i1< p1; i1++) {

		if ((double)x[nplusc] - t < 5e-6) 
		{
			t = (double)x[nplusc];
		}

		// generate the basis function for this value of t
		rbasis(k,t,npts,x,h,nbasis);

		// generate a point on the curve
		for (j = 0; j < 3; j++) 
		{
			jcount = j;
			p[icount+j] = 0.;

			// Do local matrix multiplication
			for (i = 1; i <= npts; i++) 
			{
				/* Changed by EP to facilitate reverse-order plotting */
				int point_index = reversed ? (npts - i) * 3 + j : jcount;
				temp = nbasis[i]*b[point_index];
				//temp = nbasis[i]*b[jcount];  //<< Old code
				/* End changes */

				p[icount + j] = p[icount + j] + temp;
				jcount = jcount + 3;
			}
		}
		icount = icount + 3;
		t = t + step;
	}

	delete[] x;
	delete[] nbasis;
}
void dxfSpline::rbsplinu(int npts, int k, int p1, double b[], double h[], double p[]) const
{

	int i,j,icount,jcount;
	int i1;
	//int x[30];		/* allows for 20 data points with basis function of order 5 */
	int nplusc;

	double step;
	double t;
	//double nbasis[20];
	double temp;


	nplusc = npts + k;

	int* x = new int[nplusc+1];
	double* nbasis = new double[npts+1];

	/*  zero and redimension the knot vector and the basis array */

	for(i = 0; i <= npts; i++) {
		nbasis[i] = 0.0;
	}

	for(i = 0; i <= nplusc; i++) {
		x[i] = 0;
	}

	/* generate the uniform periodic knot vector */

	knotu(npts,k,x);

	/*
		 printf("The knot vector is ");
		 for (i = 1; i <= nplusc; i++){
		 printf(" %d ", x[i]);
		 }
		 printf("\n");

		 printf("The usable parameter range is ");
		 for (i = k; i <= npts+1; i++){
		 printf(" %d ", x[i]);
		 }
		 printf("\n");
		 */

	icount = 0;

	/*    calculate the points on the rational B-spline curve */

	t = k-1;
	step = ((double)((npts)-(k-1)))/((double)(p1-1));

	for (i1 = 1; i1<= p1; i1++) {

		if ((double)x[nplusc] - t < 5e-6) {
			t = (double)x[nplusc];
		}

		rbasis(k,t,npts,x,h,nbasis);      /* generate the basis function for this value of t */
		for (j = 1; j <= 3; j++) {      /* generate a point on the curve */
			jcount = j;
			p[icount+j] = 0.;

			for (i = 1; i <= npts; i++) 
			{ /* Do local matrix multiplication */

				/* Changed by EP to facilitate reverse-order plotting */
				int point_index = reversed ? (npts - i) * 3 + j : jcount;
				temp = nbasis[i]*b[point_index];
				//temp = nbasis[i]*b[jcount];  //<< Old code
				/* End changes */

				p[icount + j] = p[icount + j] + temp;
				/*
					 printf("jcount,nbasis,b,nbasis*b,p = %d %f %f %f %f\n",jcount,nbasis[i],b[jcount],temp,p[icount+j]);
					 */
				jcount = jcount + 3;
			}
		}
		/*
			 printf("icount, p %d %f %f %f \n",icount,p[icount+1],p[icount+2],p[icount+3]);
			 */
		icount = icount + 3;
		t = t + step;
	}

	delete[] x;
	delete[] nbasis;
}

void dxfSpline::knot(int num, int order, int knotVector[]) const
{
	knotVector[1] = 0;
	for (int i = 2; i <= num + order; i++) {
		if ( (i > order) && (i < num + 2) ) {
			knotVector[i] = knotVector[i-1] + 1;
		} else {
			knotVector[i] = knotVector[i-1];
		}
	}
}

void dxfSpline::knotu(int num, int order, int knotVector[]) const
{
	int nplusc,nplus2,i;

	nplusc = num + order;
	nplus2 = num + 2;

	knotVector[1] = 0;
	for (i = 2; i <= nplusc; i++) {
		knotVector[i] = i-1;
	}
}

void dxfSpline::rbasis(int c, double t, int npts, int x[], double h[], double r[]) const
{

	int nplusc;
	int i,k;
	double d,e;
	double sum;
	//double temp[36];

	nplusc = npts + c;

	double* temp = new double[nplusc+1];

	// calculate the first order nonrational basis functions n[i]
	for (i = 1; i<= nplusc-1; i++) {
		if (( t >= x[i]) && (t < x[i+1]))
			temp[i] = 1;
		else
			temp[i] = 0;
	}

	/* calculate the higher order nonrational basis functions */

	for (k = 2; k <= c; k++) {
		for (i = 1; i <= nplusc-k; i++) {
			// if the lower order basis function is zero skip the calculation
			if (temp[i] != 0)
				d = ((t-x[i])*temp[i])/(x[i+k-1]-x[i]);
			else
				d = 0;
			// if the lower order basis function is zero skip the calculation
			if (temp[i+1] != 0)
				e = ((x[i+k]-t)*temp[i+1])/(x[i+k]-x[i+1]);
			else
				e = 0;

			temp[i] = d + e;
		}
	}

	// pick up last point
	if (t == (double)x[nplusc]) {
		temp[npts] = 1;
	}

	// calculate sum for denominator of rational basis functions
	sum = 0.;
	for (i = 1; i <= npts; i++) {
		sum = sum + temp[i]*h[i];
	}

	// form rational basis functions and put in r vector
	for (i = 1; i <= npts; i++) {
		if (sum != 0) {
			r[i] = (temp[i]*h[i])/(sum);
		} else
			r[i] = 0;
	}

	delete[] temp;
}




/* Insert */

dxfInsert::dxfInsert(const string & name, dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat sx, dxfFloat sy, dxfFloat sz, dxfFloat angle, int cols, int rows, dxfFloat colSp, dxfFloat rowSp) : name(name), insertionPoint(x,y,z), sx(sx), sy(sy), sz(sz), angle(angle), cols(cols), rows(rows), colSp(colSp), rowSp(rowSp) {}

dxfInsert::~dxfInsert(void) {}

void dxfInsert::dump(const char * prefix,ostream & out) const
{
	out << prefix << "Insert: '" << name << "'" << endl;
	out << prefix << " " << pretty_coord(insertionPoint) << " Scale: " <<pretty_coord(sx,sy,sz) << endl;
	out << prefix << " Angle: " << angle << endl;
	out << prefix << " Cols: " << cols << " Spacing: " << colSp << endl;
	out << prefix << " Rows: " << rows << " Spacing: " << rowSp << endl;
}

void dxfInsert::translate(const translateAttr & t)
{
	insertionPoint.translate(t);
}

/* MText */

dxfMText::dxfMText(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat width, dxfFloat height, int attachmentPoint, int drawingDirection, int lineSpacingStyle, dxfFloat lineSpacingFactor, const string & text, const string & style, dxfFloat angle) : insertionPoint(x,y,z), height(height), width(width), attachmentPoint(attachmentPoint), drawingDirection(drawingDirection), lineSpacingStyle(lineSpacingStyle), lineSpacingFactor(lineSpacingFactor), text(text), style(style), angle(angle) {}

dxfMText::~dxfMText(void) {}

void dxfMText::addText(const string & t) { text += t; }

void dxfMText::dump(const char * prefix,ostream & out) const
{
	out << prefix << "MText:" <<endl;
	out << prefix << " " << pretty_coord(insertionPoint) << " " << width << "x" << height << endl;
	out << prefix << " Attachment Point: " << attachmentPoint << " Drawing Direction: " << drawingDirection << endl;
	out << prefix << " Line Spacing Style: " << lineSpacingStyle << " Line Spacing Factor: " << lineSpacingFactor << endl;
	out << prefix << " Style: " << style << " Angle: " << angle << endl;
	out << prefix << " '" << text << "'" << endl;
}
void dxfMText::translate(const translateAttr & t)
{
	insertionPoint.translate(t);
}


/* Text */

dxfText::dxfText(dxfFloat ipx, dxfFloat ipy, dxfFloat ipz, dxfFloat apx, dxfFloat apy, dxfFloat apz, dxfFloat height, dxfFloat xScaleFactor, int textGenerationFlags, int hJustification, int vJustification, const string & text, const string & style, dxfFloat angle) : insertionPoint(ipx,ipy,ipz), alignmentPoint(apx,apy,apz), height(height), xScaleFactor(xScaleFactor), textGenerationFlags(textGenerationFlags), hJustification(hJustification), vJustification(vJustification), text(text), style(style), angle(angle) {}

dxfText::~dxfText(void) {}

void dxfText::translate(const translateAttr & t)
{
	insertionPoint.translate(t);
	alignmentPoint.translate(t);
}

void dxfText::dump(const char * prefix,ostream & out) const
{
	out << prefix << "Text: Flags: 0x" << hex << textGenerationFlags << dec << endl;
	out << prefix << " Insertion Point: " << pretty_coord(insertionPoint) << " Alignment Point " << pretty_coord(alignmentPoint) << endl;
	out << prefix << " X Scale Factor: " << xScaleFactor << " Height: " << height << endl;
	out << prefix << " H. Justification: " << hJustification << " V. Justification: " << vJustification << endl;
	out << prefix << " Angle: " << angle << " Style: " << style <<endl;
	out << prefix << " '" << text << "'" << endl;
}

/* Dim*/


dxfDim::dxfDim(dxfFloat dpx, dxfFloat dpy, dxfFloat dpz, dxfFloat mpx, dxfFloat mpy, dxfFloat mpz, int type, int attachmentPoint, int lineSpacingStyle, dxfFloat lineSpacingFactor, const string & text, const string & style, dxfFloat angle, dxfObject * subtype_data) :
	definitionPoint(dpx,dpy,dpz),
	middlePoint(mpx,mpy,mpz),
	type(type),
	attachmentPoint(attachmentPoint),
	lineSpacingStyle(lineSpacingStyle),
	lineSpacingFactor(lineSpacingFactor),
	text(text),
	style(style),
	angle(angle),
	subtype_data(subtype_data)
{}

dxfDim::~dxfDim(void)
{
	if (subtype_data != NULL)
		delete subtype_data;
}

void dxfDim::translate(const translateAttr & t)
{
	definitionPoint.translate(t);
	middlePoint.translate(t);
}

void dxfDim::dump(const char * prefix,ostream & out) const
{

	static const char * types[] =
	{
		"Rotated, horizontal, or vertical",
		"Aligned",
		"Angular",
		"Diametric",
		"Radius",
		"Angular 3-point",
		"Ordinate"
	};
	/*
		 64 Ordinate type. This is a bit value (bit 7) used only with integer value 6. If set, ordinate is X-type; if not set, ordinate is Y-type 128 This is a bit value (bit 8) added to the other group 70 values if the dimension text has been positioned at a user-defined location rather than at the default location
		 */
	out << prefix << "Dim ";
	if (subtype_data != NULL)
	{
		if (typeid(*subtype_data) == typeid(dxfDimAlign))
			out << "(Align)";
		else if (typeid(*subtype_data) == typeid(dxfDimLinear))
			out << "(Linear)";
		else if (typeid(*subtype_data) == typeid(dxfDimRadial))
			out << "(Radial)";
		else if (typeid(*subtype_data) == typeid(dxfDimDiametric))
			out << "(Diametric)";
		else if (typeid(*subtype_data) == typeid(dxfDimAngular))
			out << "(Angular)";
		else if (typeid(*subtype_data) == typeid(dxfDimAngular3p))
			out << "(Angular3p)";
		else
			out << "(Uknown Subtype)";
	}
	else
		out << "(NULL Subtype)";

	out << endl;
	out << prefix << " Definition Point: " << pretty_coord(definitionPoint) << " Middle Point: " << pretty_coord(middlePoint);
	out << "Attachment Point: " << attachmentPoint << endl;
	out << prefix << " Type: " << types[type & 0x07] << " (0x" << hex << type << dec << ")";

	if ((type & 0x07) == 6)
	{ // Ordinate type
		out << ((type & 0x40) ? " X type" : " Y type");
		out << ((type & 0x80) ? " User-defined position" : " Default position");
	}

	out << prefix << " Line SpacingStyle: " << lineSpacingStyle << " Line Spacing Factor: " << lineSpacingFactor << endl;
	out << prefix << " Style: " << style << " Angle: " << angle;
	out << prefix << " '" << text << "'" << endl;
	if (subtype_data != NULL)
	{
		out << prefix << " Subtype Dump:" << endl;
		string p = prefix;
		p += "  ";
		subtype_data->dump(p.c_str(),out);
	}


}

/* DimAlign */


dxfDimAlign::dxfDimAlign(dxfFloat x1, dxfFloat y1, dxfFloat z1, dxfFloat x2, dxfFloat y2, dxfFloat z2) :
	p1(x1,y1,z1),
	p2(x2,y2,z2)
{}

dxfDimAlign::~dxfDimAlign(void) {}
void dxfDimAlign::dump(const char * prefix,ostream & out) const
{
	out << prefix << "DimAlign: " << pretty_coord(p1) << " to " << pretty_coord(p2) << endl;
}
void dxfDimAlign::translate(const translateAttr & t)
{
	p1.translate(t);
	p2.translate(t);
}


/* DimLinear */


dxfDimLinear::dxfDimLinear(dxfFloat x1, dxfFloat y1, dxfFloat z1, dxfFloat x2, dxfFloat y2, dxfFloat z2, dxfFloat angle, dxfFloat oblique) :
	p1(x1,y1,z1),
	p2(x2,y2,z2),
	angle(angle),
	oblique(oblique) 
{}

dxfDimLinear::~dxfDimLinear(void) {}
void dxfDimLinear::translate(const translateAttr & t)
{
	p1.translate(t);
	p2.translate(t);
}

void dxfDimLinear::dump(const char * prefix,ostream & out) const
{
	out << prefix << "DimLinear: " << pretty_coord(p1) << " to " << pretty_coord(p2) << " Angle: " << angle << " Oblique: " << oblique << endl;
}

/* DimRadial */

dxfDimRadial::dxfDimRadial(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat leader) :
	center(x,y,z),
	leader(leader) 
{}

dxfDimRadial::~dxfDimRadial(void)
{}
void dxfDimRadial::dump(const char * prefix,ostream & out) const
{
	out << prefix << "DimRadial: " << pretty_coord(center) << " Leader: " << leader <<endl;
}

void dxfDimRadial::translate(const translateAttr & t)
{
	center.translate(t);
}

/* DimDiametric */


dxfDimDiametric::dxfDimDiametric(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat leader) :
	p(x,y,z),
	leader(leader) 
{}

dxfDimDiametric::~dxfDimDiametric(void)
{}
void dxfDimDiametric::dump(const char * prefix,ostream & out) const
{
	out << prefix << "DimDiametric: " << pretty_coord(p) << " Leader: " << leader <<endl;
}
void dxfDimDiametric::translate(const translateAttr & t)
{
	p.translate(t);
}


/* DimAngular */

dxfDimAngular::dxfDimAngular(dxfFloat x1, dxfFloat y1, dxfFloat z1, dxfFloat x2, dxfFloat y2, dxfFloat z2, dxfFloat x3, dxfFloat y3, dxfFloat z3, dxfFloat x4, dxfFloat y4, dxfFloat z4) :

	p1(x1,y1,z1),
	p2(x2,y2,z2),
	p3(x3,y3,z3),
	p4(x4,y4,z4)
{}

dxfDimAngular::~dxfDimAngular(void) {}

void dxfDimAngular::dump(const char * prefix,ostream & out) const
{
	out << prefix << "DimAngular: " << 
		pretty_coord(p1) << " / " <<
		pretty_coord(p2) << " / " <<
		pretty_coord(p3) << " / " <<
		pretty_coord(p4) << endl;
}
void dxfDimAngular::translate(const translateAttr & t)
{
	p1.translate(t);
	p2.translate(t);
	p3.translate(t);
	p4.translate(t);
}

/* DimAngular3p */


dxfDimAngular3p::dxfDimAngular3p(dxfFloat x1, dxfFloat y1, dxfFloat z1, dxfFloat x2, dxfFloat y2, dxfFloat z2, dxfFloat x3, dxfFloat y3, dxfFloat z3) :
	p1(x1,y1,z1),
	p2(x2,y2,z2),
	p3(x3,y3,z3)
{}

dxfDimAngular3p::~dxfDimAngular3p(void) {}
void dxfDimAngular3p::dump(const char * prefix,ostream & out) const
{
	out << prefix << "DimAngular3p: " << 
		pretty_coord(p1) << " / " <<
		pretty_coord(p2) << " / " <<
		pretty_coord(p3) << endl;
}
void dxfDimAngular3p::translate(const translateAttr & t)
{
	p1.translate(t);
	p2.translate(t);
	p3.translate(t);
}

/* Leader */


dxfLeader::dxfLeader(int arrowHeadFlag, int leaderPathType, int leaderCreationFlag, int hooklineDirectionFlag, int hooklineFlag, dxfFloat textAnnotationHeight, dxfFloat textAnnotationWidth) :
	arrowHeadFlag(arrowHeadFlag),
	leaderPathType(leaderPathType),
	leaderCreationFlag(leaderCreationFlag),
	hooklineDirectionFlag(hooklineDirectionFlag),
	hooklineFlag(hooklineFlag),
	textAnnotationHeight(textAnnotationHeight),
	textAnnotationWidth(textAnnotationWidth)
{}

dxfLeader::~dxfLeader(void) {}

void dxfLeader::addVertex( const dxfPoint & p) {vertices.push_back(p);}

void dxfLeader::dump(const char * prefix,ostream & out) const
{
	out << prefix << "Leader: " << endl;
	out << prefix << " Arrow Head Flag: 0x" << hex << arrowHeadFlag << dec << endl;
	out << prefix << " Leader Path Type: " << leaderPathType << " Leader Creation Flag: 0x" << hex << leaderCreationFlag << dec << endl;
	out << prefix << " Hook Line Dir Flag: " << hooklineDirectionFlag << " Hook Line Flag: " << hooklineFlag << endl;
	out << prefix << " Text Anno. Height: " << textAnnotationHeight << " Text Anno. Width: " << textAnnotationWidth << endl;
	out << prefix << " Vertices: " << endl;
	for (dxfPointVector::const_iterator i = vertices.begin(); i != vertices.end(); i++)
		out << prefix << "  " << pretty_coord(i->x,i->y,i->z) << endl;
}
void dxfLeader::translate(const translateAttr & t)
{
	for (dxfPointVector::iterator i = vertices.begin(); i != vertices.end(); i++)
		*i = translatePoint(*i,t);
}


/* Hatch */

dxfHatch::dxfHatch(bool solid, dxfFloat scale, dxfFloat angle, const string & pattern) :
	solid(solid),
	scale(scale),
	angle(angle),
	pattern(pattern) {}

	dxfHatch::~dxfHatch(void) {}

void dxfHatch::addLoop(dxfObjectPtr l) 
{
	if (typeid(*l) == typeid(dxfHatchLoop))
		loops.push_back(l); 
}

void dxfHatch::dump(const char * prefix,ostream & out) const
{
	out << prefix << "Hatch: " << endl;
	out << prefix << " Solid: " << solid << " Scale: " << scale << " Angle: " << angle << "Pattern: '" << pattern << "'" << endl;
	out << prefix << " Loops:" << endl;
	string p = prefix;
	p += "  ";
	for(dxfObjectPtrVector::const_iterator i = loops.begin(); i != loops.end(); i++)
		(**i).dump(p.c_str(),out);
}

/* dxfHatchLoop */

dxfHatchLoop::dxfHatchLoop(void) {}
dxfHatchLoop::~dxfHatchLoop(void) {}

void dxfHatchLoop::addEdge(dxfObjectPtr e) 
{
	if (typeid(*e) == typeid(dxfHatchEdge))
		edges.push_back(e); 
}

void dxfHatchLoop::dump(const char * prefix,ostream & out) const
{
	out << prefix << "HatchLoop: " << endl;
	out << prefix << " Edges:" << endl;
	string p = prefix;
	p += "  ";
	for(dxfObjectPtrVector::const_iterator i = edges.begin(); i != edges.end() ; i ++)
		(**i).dump(p.c_str(),out);
}

/* dxfHatchEdge */

dxfHatchEdge::dxfHatchEdge(int type, bool defined, dxfFloat x1, dxfFloat y1, dxfFloat x2, dxfFloat y2, dxfFloat cx, dxfFloat cy, dxfFloat radius, dxfFloat angle1, dxfFloat angle2, bool ccw) : 
	type(type),
	defined(defined),
	p1(x1,y1,0),
	p2(x2,y2,0),
	center(cx,cy,0),
	radius(radius),
	angle1(angle1),
	angle2(angle2),
	ccw(ccw) 
{}

dxfHatchEdge::~dxfHatchEdge(void) {}
void dxfHatchEdge::dump(const char * prefix,ostream & out) const
{
	out << prefix << "HatchEdge: " << endl;
	out << prefix << " " << pretty_coord(p1) << " to " << pretty_coord(p2) << "Arc Center: " << pretty_coord(center) << endl;
	out << prefix << " Radius: " << radius << " Angle 1: " << angle1 << " Angle 2: " << angle2 << (ccw ? "Counter Clockwise" : "Clockwise") << endl;
}
void dxfHatchEdge::translate(const translateAttr & t)
{
	p1.translate(t);
	p2.translate(t);
	center.translate(t);
}
/* Image */

dxfImage::dxfImage(const string & ref, dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat ux, dxfFloat uy, dxfFloat uz, dxfFloat vx, dxfFloat vy, dxfFloat vz, int width, int height, int brightness, int contrast, int fade) : 
	ref(ref),
	insertionPoint(x,y,z),
	u(ux,uy,uz),
	v(vx,vy,vz),
	width(width),
	height(height),
	brightness(brightness),
	contrast(contrast),
	fade(fade) {}

	dxfImage::~dxfImage(void) {}
	void dxfImage::dump(const char * prefix,ostream & out) const
{
	out << prefix << "Image: " << pretty_coord(insertionPoint) << " U Vector: " << pretty_coord(u) << " V Vector: " << pretty_coord(v) << endl;
	out << prefix <<  " " << width << "x" << height << " Bright: " << brightness << " Contrast: " << contrast << " Fade: " << fade << endl;
	out << prefix << " Ref: '" << ref << "'" << endl;
};
void dxfImage::translate(const translateAttr & t)
{
	insertionPoint.translate(t);
	u.translate(t);
	v.translate(t);
}

/* ImageLink */


dxfImageLink::dxfImageLink(const string & ref, const string & file) : ref(ref), file(file) {}
dxfImageLink::~dxfImageLink(void) {}

void dxfImageLink::dump(const char * prefix,ostream & out) const
{
	out << prefix << "ImageLink: Ref: '" << ref << "' File: '" << file << "'" << endl; 
};


/* VariableVector */

dxfVariableVector::dxfVariableVector(const string & name, dxfFloat x, dxfFloat y, dxfFloat z, int xxx) :
	name(name),
	p(x,y,z),
	xxx(xxx) {}

	dxfVariableVector::~dxfVariableVector(void) {}

	void dxfVariableVector::dump(const char * prefix,ostream & out) const
{
	out << prefix << "VariableVector: '" << name << "' " << pretty_coord(p) << endl;
}
void dxfVariableVector::translate(const translateAttr & t)
{
	p.translate(t);
}

/* VariableString */

dxfVariableString::dxfVariableString(const string & name, const string & value, int xxx) :
	name(name),
	value(value),
	xxx(xxx) {}

	dxfVariableString::~dxfVariableString(void) {}

	void dxfVariableString::dump(const char * prefix,ostream & out) const
{
	out << prefix << "VariableString: '" << name << "' = '" << value << "'" << endl;
}

/* VariableInt */

dxfVariableInt::dxfVariableInt(const string & name, int value, int xxx) :
	name(name),
	value(value),
	xxx(xxx) {}

	dxfVariableInt::~dxfVariableInt(void) {}

	void dxfVariableInt::dump(const char * prefix,ostream & out) const
{
	out << prefix << "VariableInt: '" << name << "' = " << value << endl;
}

/* VariableDouble */

dxfVariableDouble::dxfVariableDouble(const string & name, dxfFloat value, int xxx) :
	name(name),
	value(value),
	xxx(xxx) {}

	dxfVariableDouble::~dxfVariableDouble(void) {}

	void dxfVariableDouble::dump(const char * prefix,ostream & out) const
{
	out << prefix << "VariableDouble: '" << name << "' = " << value << endl;
}












