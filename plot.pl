#!/usr/bin/perl


use Image::Magick;
use Data::Dumper;
use Math::Trig;
use strict;


my $pi = 3.141592653589793238462643383279;

my @points;





my $x_min = 999999;
my $y_min = 999999;
my $x_max = -999999;
my $y_max = -999999;

my $border = 10;
my $factor = 200;

my @file = <>;

foreach (@file)
{
  if (/^(?:BEGIN|CUT|RAPID):.*?\[(.*?)\]$/)
  {
    push @points,[split (/\s*,\s*/,$1)];
  }
}

foreach my $point(@points)
{
  my ($x,$y,$z) = @$point;
  $x_min = min($x_min,$x);
  $y_min = min($y_min,$y);
  $x_max = max($x_max,$x);
  $y_max = max($y_max,$y);
}

print "x_min: $x_min y_min: $y_min\n";
my $x_off = -$x_min*$factor;
my $y_off = -$y_min*$factor;

print "x_off: $x_off y_off: $y_off\n";

foreach my $point (@points)
{
  @$point[0,1,2] = translate(@$point[0,1,2],$x_off,$y_off,$factor,$border);
}

my $draw_width = ($x_max - $x_min) * $factor;
my $draw_height = ($y_max - $y_min) * $factor;
my $image_width = $draw_width  + $border *2;
my $image_height = $draw_height + $border *2;

my $point_string =  join(' ',map {join (',',@$_[0,1])} @points);
my $i = Image::Magick->new(size=>$image_width.'x'.$image_height);
print "Offsets: x: $x_off y: $y_off\n";
print "Width: $image_width Height: $image_height\n";
print $point_string,"\n";
$i->ReadImage('xc:white');

#my $circle_points = ($points[0][0] + 5).",".($points[0][1]) . "$points[0][0],$points[0][1] ";


#my $p1 = join (',',@{$points[0]}[0,1]);
#my $p2 = join (',',map {$_+5}@{$points[0]}[0,1]);
#$i->Draw (primitive=> 'circle', points=> "$p1 $p2", stroke=>"blue");


my $gridsize = $factor /10;
my $x_gridlines = $draw_width / $gridsize + 1;
my $y_gridlines = $draw_height / $gridsize + 1;

my $gridcolor = "yellow";
for (my $x = $border; $x <= $x_gridlines* $gridsize; $x += $gridsize)
{
  $i->Draw
    (
     primitive => "line",
     points => "$x,0 $x,$image_height",
     stroke => $gridcolor
    );
}
for (my $y = $border; $y <= $y_gridlines * $gridsize; $y += $gridsize)
{
  $i->Draw
    (
     primitive => "line",
     points => "0,$y $image_width,$y",
     stroke => $gridcolor
    );
}

#$i->Draw( primitive=>'polyline', points=> $point_string, stroke=> 'black' );

my %colors = (CUT=>'BLACK', RAPID=>'BLUE', PLUNGE=>'RED', LIFT => 'GREEN', BEGIN=> '#a0a0a0');
my $last_point;
my @targets;

print Dumper \%colors;

foreach (@file)
{
	my ($type) = m/(BEGIN|CUT|RAPID|PLUNGE|LIFT)/;

	my ($x,$y,$z) = translate(map {trim($_)} m/\[(.+),(.+),(.+)\]/, $x_off,$y_off,$factor,$border);

	print "$type $x, $y, $z" .($last_point ? " ($$last_point[0],$$last_point[1])" : "")."\n";

	if ($type eq 'BEGIN')
	{
	 push @targets,[$x,$y,3,5,45,$colors{BEGIN}];
	 $last_point = [$x,$y];
	}
	elsif ($type eq 'CUT')
	{
	 $i->Draw(primitive=>'line',stroke=>$colors{CUT}, points=>"$$last_point[0],$$last_point[1] $x,$y") if ($last_point);
	 $last_point = [$x,$y];
	}
	elsif ($type eq 'RAPID')
	{
	 $i->Draw(primitive=>'line',stroke=>$colors{RAPID}, points=>"$$last_point[0],$$last_point[1] $x,$y") if ($last_point);
	 $last_point = [$x,$y];
	}
	elsif ($type eq 'PLUNGE')
	{
	 push @targets,[@$last_point,3,5,0,$colors{PLUNGE}];
	}
	elsif ($type eq 'LIFT')
	{
	 push @targets,[@$last_point,3,5,45,$colors{LIFT}];
	}

}
foreach (@targets)
{
 print "@$_\n";
 target($i,@$_);
}


$i->Flip();
$i->Write("test.png");

sub translate
{
  my ($x,$y,$z,$x_off,$y_off,$factor,$border) = @_;
  return ($x*$factor + $x_off + $border,$y * $factor+$y_off + $border, $z * $factor);
}




sub min { return (sort {$a <=> $b} @_)[0] }
sub max { return (sort {$b <=> $a} @_)[0] }

sub target
{
  my ($i,$x,$y,$inner_rad,$outer_rad,$rot,$color) = @_;
  my $angle = 10;
  my @p;
  push @p,[vector($x,$y,$inner_rad, $rot + 0 )];
  push @p,[vector($x,$y,$outer_rad, $rot + $angle)];
  push @p,[vector($x,$y,$outer_rad, $rot - $angle)];
  push @p,[@{$p[0]}];

  my $p = join (' ', map {round($$_[0],0).','.round($$_[1],0)} @p);
  $i->Draw(primitive=>'polygon', stroke=>$color, fill =>$color, method=>'Floodfill',points=>$p);

	map {$_ = [rpoint($x,$y,@$_,90)]} @p;
  my $p = join (' ', map {round($$_[0],0).','.round($$_[1],0)} @p);
  $i->Draw(primitive=>'polygon', stroke=>$color, fill =>$color, method=>'Floodfill',points=>$p);

	map {$_ = [rpoint($x,$y,@$_,90)]} @p;
  my $p = join (' ', map {round($$_[0],0).','.round($$_[1],0)} @p);
  $i->Draw(primitive=>'polygon', stroke=>$color, fill =>$color, method=>'Floodfill',points=>$p);

	map {$_ = [rpoint($x,$y,@$_,90)]} @p;
  my $p = join (' ', map {round($$_[0],0).','.round($$_[1],0)} @p);
  $i->Draw(primitive=>'polygon', stroke=>$color, fill =>$color, method=>'Floodfill',points=>$p);


}


sub round 
{
 my ($x,$digits) = @_;
 my $fact = 10 ** $digits;
 int($x * $fact) / $fact;
}

sub mod
{
 my ($x,$y) = @_;

 my $q = int($x/$y);
 return $x - ($q * $y);
}


sub dist
{
  my ($x1,$y1,$x2,$y2) = @_;
	return sqrt(($x2-$x1) ** 2 + ($y2-$y1) ** 2);
}

sub calc_angle
{
	my ($cx,$cy,$x,$y) = @_;	


	my @quadrature = (0,90,270,180);
	my @flip = (0,90,90,0);
	my $ydiff = $y - $cy;
	my $xdiff = $x - $cx;

	my $xquad = $xdiff < 0 ? 1 : 0; 
	my $yquad = $ydiff < 0 ? 2 : 0;
	## 0 = +x, +y == Q1 0-90
	## 1 = -x, +y == Q2 90-180
	## 2 = +x, -y == Q4 270-360
	## 3 = -x, -y == Q3 180-270
	
	my $q = $xquad+$yquad;

	return 0 if ($xdiff == 0 && $ydiff == 0);
	return $ydiff >= 0 ? 90 : 270 if ($xdiff == 0); # avoid divide by zero

	$xdiff = abs($xdiff);
	$ydiff = abs($ydiff);
	my $ratio = round($ydiff / $xdiff, 5);
	my $angle = rad2deg(atan($ratio));
	$angle = abs($angle - $flip[$q]) + $quadrature[$q];

	#print "cangle: $cx,$cy,$x,$y = $angle\n";
 return $angle;

}

sub rpoint
{
 my ($cx,$cy,$x,$y,$a) = @_;

 my $radius = dist ($cx,$cy,$x,$y);
 my $angle  = calc_angle($cx,$cy,$x,$y);
 return vector($cx,$cy,$radius,$angle+$a);

}
sub vector
{
  my ($x,$y,$r,$a) = @_;
	my $aa = deg2rad($a);
	return ($x + $r * cos($aa), $y + $r * sin($aa));
}

sub trim
{
 my $x = shift;
 $x =~ s/^\s*(.*?)\s*$/$1/;
 return $x;
}
