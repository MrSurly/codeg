/*
 * Codeg DXF to GCODE convertor
 * Copyright (C) 2006 Eric Poulsen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 *
 * Author contact info:
 *
 * Eric Poulsen
 * email: eric@zyxod.com
 *   AIM: epoulsen2
 *   ICQ: 1608780
 *
 */

#ifndef DXF_HPP
#define DXF_HPP
#include <string>
#include <vector>
#include <iostream>

#include <memory>

#include <m_apm.h> // Arbitrary precision library


using namespace std;



/* Predeclarations */

class dxfPoint;
class dxfHatchLoop;
class dxfHatchEdge;
class dxfObject;
class dxfPolylineVertex;
class translateAttr;

/* typedefs */

typedef MAPM dxfFloat;


ostream & operator << (ostream & os, const dxfFloat & x);

typedef shared_ptr < dxfObject > dxfObjectPtr;

typedef vector < dxfPoint > dxfPointVector;
typedef vector < dxfFloat > dxfFloatVector;
typedef vector < dxfHatchLoop > dxfHatchLoopVector;
typedef vector < dxfHatchEdge > dxfHatchEdgeVector;
typedef vector < dxfPolylineVertex > dxfPolylineVertexVector;

typedef vector < dxfObjectPtr > dxfObjectPtrVector;
typedef vector < dxfObjectPtrVector > dxfObjectPtrVectorVector;

class dxfObject
{
  public: 
    dxfObject(void);
		virtual ~dxfObject(void);
    virtual void dump(const char * prefix = "", ostream & out = cout) const;
    virtual void push_back(dxfObjectPtr o);

    virtual bool isContainer(void);
    virtual dxfObjectPtrVector::iterator begin(void);
    virtual dxfObjectPtrVector::iterator end(void);
    virtual bool isDrawable(void);
    virtual void toPoints(dxfPointVector & out, const translateAttr & translate, bool output_first_point = false) const;
    virtual void toGcode(ostream & out, int & line_num, const translateAttr & translate, bool output_first_point = false) const;
    virtual bool reverse(void);
    virtual dxfPoint beginPoint(void) const;
    virtual dxfPoint endPoint(void) const;
    virtual void translate(const translateAttr & t);

    static dxfFloat deg2rad(dxfFloat d);
    static dxfFloat rad2deg(dxfFloat r);
    static dxfFloat mod(dxfFloat x, dxfFloat m);
    static dxfFloat distance2D(const dxfPoint & p1, const dxfPoint & p2);
    static dxfPoint calcVector(dxfPoint center, dxfFloat radius, dxfFloat angle);
    static string pretty_coord (dxfFloat x, dxfFloat y, dxfFloat z);
    static string pretty_coord (const dxfPoint & p);
    static dxfFloat abs(dxfFloat q);
    static dxfPoint rotatePoint2D(const dxfPoint & center, const dxfPoint & p, dxfFloat angle);
    static dxfFloat calcAngle(const dxfPoint & from, const dxfPoint & to);
		static dxfFloat angleDiff(const dxfFloat a1, const dxfFloat a2);
    static dxfFloat min(dxfFloat x, dxfFloat y);
    static dxfFloat max(dxfFloat x, dxfFloat y);
    static dxfPoint translatePoint(const dxfPoint & p, const translateAttr & t);
    static void gcodePoint(ostream & out, const dxfPoint & p, const translateAttr & t);
    static void printGcodeLine(ostream & out, int & line_num, int inc = 5);



    bool reversed;
};

class dxfDoc : public dxfObject
{
  public:
    dxfDoc(void);
    ~dxfDoc(void);
    void dump(const char * prefix = "", ostream & out = cout) const;
    void push_back(dxfObjectPtr p);
    bool isContainer(void);
    void translate(const translateAttr & t);
    dxfObjectPtrVector::iterator begin(void);
    dxfObjectPtrVector::iterator end(void);

    dxfObjectPtrVector objects;

};


class dxfLayer : public dxfObject
{
  public:

    dxfLayer(const string & name, int flags);
    ~dxfLayer(void);

    void dump(const char * prefix = "", ostream & out = cout) const;
    void push_back(dxfObjectPtr o);
    bool isContainer(void);
    void translate(const translateAttr & t);
    dxfObjectPtrVector::iterator begin(void);
    dxfObjectPtrVector::iterator end(void);

    string name;
    int flags;
		bool visible;
    dxfObjectPtrVector objects;

};

class dxfBlock : public dxfObject
{
  public:

    dxfBlock(const string & name, int flags, dxfFloat x, dxfFloat y, dxfFloat z);
    ~dxfBlock(void);

    void translate(const translateAttr & t);
    void dump(const char * prefix = "", ostream & out = cout) const;
    void push_back(dxfObjectPtr o);
    bool isContainer(void);
    dxfObjectPtrVector::iterator begin(void);
    dxfObjectPtrVector::iterator end(void);

    string name;
    int flags;
    dxfFloat x;
    dxfFloat y;
    dxfFloat z;
    dxfObjectPtrVector objects;
};

#define DXFPOINT_ROUND 5
#define DXFPOINT_EQUAL_THRESHOLD 10e-5

class dxfPoint : public dxfObject
{
  public:

    dxfPoint(dxfFloat x, dxfFloat y, dxfFloat z);
    dxfPoint(void);
    ~dxfPoint(void);

    bool operator == (const dxfPoint & p);
    bool operator != (const dxfPoint & p) {return !(*this == p); }


    void dump(const char * prefix = "", ostream & out = cout) const;
    bool isDrawable(void);
    void translate(const translateAttr & t);
    void toPoints(dxfPointVector & out, const translateAttr & t, bool output_first_point = false) const;
    void toGcode(ostream & out, int & line_num, const translateAttr & translate, bool output_first_point = false) const;
    void round(int places = 6);
    dxfPoint beginPoint(void) const;
    dxfPoint endPoint(void) const;

    dxfFloat x;
    dxfFloat y;
    dxfFloat z;


};

class translateAttr
{
  public:

    translateAttr(void);

    dxfPoint xy_center;
    dxfPoint yz_center;
    dxfPoint xz_center;

    dxfFloat xy_angle;
    dxfFloat yz_angle;
    dxfFloat xz_angle;

    dxfPoint offset;
    bool x_enable;
    bool y_enable;
    bool z_enable;

    dxfFloat max_line_seg;
};


class dxfLine : public dxfObject
{
  public:

		dxfLine();
    dxfLine(dxfFloat x1, dxfFloat y1, dxfFloat z1, dxfFloat x2, dxfFloat y2, dxfFloat z2);
		dxfLine(const dxfPoint & point1,const dxfPoint & point2);
    ~dxfLine(void);


    void dump(const char * prefix = "", ostream & out = cout) const;
    void translate(const translateAttr & t);
    bool isDrawable(void);
    void toPoints(dxfPointVector & out, const translateAttr & t, bool output_first_point = false) const;
    void toGcode(ostream & out, int & line_num, const translateAttr & translate, bool output_first_point = false) const;
		dxfPoint nearestPoint(const dxfPoint & p);
    dxfPoint beginPoint(void) const;
    dxfPoint endPoint(void) const;

    dxfPoint p1;
    dxfPoint p2;
};

class dxfArc : public dxfObject
{
  public:

    dxfArc(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat radius, dxfFloat angle1, dxfFloat angle2);
    ~dxfArc(void);


    void dump(const char * prefix = "", ostream & out = cout) const;
    void translate(const translateAttr & t);
    bool isDrawable(void);
    void toPoints(dxfPointVector & out, const translateAttr & t, bool output_first_point = false) const;
    void toGcode(ostream & out, int & line_num, const translateAttr & translate, bool output_first_point = false) const;
    dxfPoint beginPoint(void) const;
    dxfPoint endPoint(void) const;


    dxfPoint    center;

    dxfFloat 	radius;
    dxfFloat 	angle1;
    dxfFloat 	angle2;

    dxfPoint	end_point_1;
    dxfPoint    end_point_2;

};

class dxfCircle : public dxfObject
{
  public:

    dxfCircle(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat radius);
    ~dxfCircle(void);


    void dump(const char * prefix = "", ostream & out = cout) const;
    void translate(const translateAttr & t);
    bool isDrawable(void);
    void toPoints(dxfPointVector & out, const translateAttr & t, bool output_first_point = false) const;
    void toGcode(ostream & out, int & line_num, const translateAttr & translate, bool output_first_point = false) const;
    dxfPoint beginPoint(void) const;
    dxfPoint endPoint(void) const;

    dxfPoint center;

    dxfFloat 	radius;
};

class dxfEllipse : public dxfObject
{
  public:

    dxfEllipse(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat mx, dxfFloat my, dxfFloat mz, dxfFloat ratio, dxfFloat angle1, dxfFloat angle2);
    ~dxfEllipse(void);

    dxfPoint plotPoint(dxfFloat angle) const;


    void dump(const char * prefix = "", ostream & out = cout) const;
    void translate(const translateAttr & t);
    bool isDrawable(void);
    void toPoints(dxfPointVector & out,	const translateAttr & t, bool output_first_point = false) const;
    void toGcode(ostream & out, int & line_num, const translateAttr & translate, bool output_first_point = false) const;
    dxfPoint beginPoint(void) const;
    dxfPoint endPoint(void) const;

    dxfPoint center;
    dxfPoint major_endpoint;

    dxfFloat ratio;
    dxfFloat angle1;
    dxfFloat angle2;

    dxfFloat major_radius;
    dxfFloat minor_radius;
    dxfFloat tilt_angle;

    dxfPoint	end_point_1;
    dxfPoint    end_point_2;

};

class dxfPolyline : public dxfObject
{
  public:

    dxfPolyline(int flags);
    ~dxfPolyline(void);

    void addVertex(const dxfPolylineVertex & v);

    void dump(const char * prefix = "", ostream & out = cout) const;
    void translate(const translateAttr & t);
    bool isDrawable(void);
    void toPoints(dxfPointVector & out, const translateAttr & t, bool output_first_point = false) const;
    void toGcode(ostream & out, int & line_num, const translateAttr & translate, bool output_first_point = false) const;
    dxfPoint beginPoint(void) const;
    dxfPoint endPoint(void) const;

    int 	flags;

    dxfPolylineVertexVector vertices;


};

class dxfPolylineVertex : public dxfObject
{
  public:
    dxfPolylineVertex(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat bulge);
    ~dxfPolylineVertex(void);
    void translate(const translateAttr & t);
    void dump(const char * prefix = "",ostream & out = cout) const;

    dxfPoint p;
    dxfFloat bulge;
};


class dxfSpline : public dxfObject
{
  public:

    dxfSpline(int flags, int degree, bool closed = false);
    ~dxfSpline(void);

    void addKnot (dxfFloat k);
    void addPoint (const dxfPoint & p);

    void dump(const char * prefix = "", ostream & out = cout) const;

    void translate(const translateAttr & t);
    bool isDrawable(void);
    void toPoints(dxfPointVector & out, const translateAttr & t, bool output_first_point = false) const;
    void toGcode(ostream & out, int & line_num, const translateAttr & translate, bool output_first_point = false) const;
    void testPlot(void);
    dxfPoint beginPoint(void) const;
    dxfPoint endPoint(void) const;

    void plot (dxfFloat line_seg_max = .05);

    void rbspline(int npts, int k, int p1, double b[], double h[], double p[]) const;
    void rbsplinu(int npts, int k, int p1, double b[], double h[], double p[]) const;
    void knot(int num, int order, int knotVector[]) const; 
    void knotu(int num, int order, int knotVector[]) const; 
    void rbasis(int c, double t, int npts, int x[], double h[], double r[]) const;



    int            flags;
    int            degree;
    bool           closed;
    dxfFloatVector knots;
    dxfPointVector points;
};


class dxfInsert : public dxfObject
{
  public:

    dxfInsert(const string & name, dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat sx, dxfFloat sy, dxfFloat sz, dxfFloat angle, int cols, int rows, dxfFloat colSp, dxfFloat rowSp);
    ~dxfInsert(void);


    void translate(const translateAttr & t);
    void dump(const char * prefix = "", ostream & out = cout) const;

    string 	name;
    dxfPoint insertionPoint;
    dxfFloat 	sx;
    dxfFloat 	sy;
    dxfFloat 	sz;
    dxfFloat 	angle;
    int 	cols;
    int 	rows;
    dxfFloat 	colSp;
    dxfFloat 	rowSp;


};

class dxfMText : public dxfObject
{
  public:

    dxfMText(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat width, dxfFloat height, int attachmentPoint, int drawingDirection, int lineSpacingStyle, dxfFloat lineSpacingFactor, const string & text, const string & style, dxfFloat angle);
    ~dxfMText(void);

    void addText(const string & t);


    void translate(const translateAttr & t);
    void dump(const char * prefix = "", ostream & out = cout) const;

    dxfPoint insertionPoint;
    dxfFloat 	height;
    dxfFloat 	width;
    int 	attachmentPoint;
    int 	drawingDirection;
    int 	lineSpacingStyle;
    dxfFloat 	lineSpacingFactor;
    string 	text;
    string 	style;
    dxfFloat 	angle;


};

class dxfText : public dxfObject
{
  public:

    dxfText(dxfFloat ipx, dxfFloat ipy, dxfFloat ipz, dxfFloat apx, dxfFloat apy, dxfFloat apz, dxfFloat height, dxfFloat xScaleFactor, int textGenerationFlags, int hJustification, int vJustification, const string & text, const string & style, dxfFloat angle);

    ~dxfText(void);

    void dump(const char * prefix = "", ostream & out = cout) const;

    void translate(const translateAttr & t);

    dxfPoint insertionPoint;
    dxfPoint alignmentPoint;
    dxfFloat 	height;
    dxfFloat 	xScaleFactor;
    int 	textGenerationFlags;
    int 	hJustification;
    int 	vJustification;
    string 	text;
    string 	style;
    dxfFloat 	angle;

};

class dxfDim : public dxfObject
{
  public:

    dxfDim(dxfFloat dpx, dxfFloat dpy, dxfFloat dpz, dxfFloat mpx, dxfFloat mpy, dxfFloat mpz, int type, int attachmentPoint, int lineSpacingStyle, dxfFloat lineSpacingFactor, const string & text, const string & style, dxfFloat angle, dxfObject * subtype_data);
    ~dxfDim(void);


    void dump(const char * prefix = "", ostream & out = cout) const;
    void translate(const translateAttr & t);

    dxfPoint definitionPoint;
    dxfPoint middlePoint;
    int 	type;
    int 	attachmentPoint;
    int 	lineSpacingStyle;
    dxfFloat 	lineSpacingFactor;
    string 	text;
    string 	style;
    dxfFloat 	angle;

    dxfObject * subtype_data;



};

class dxfDimAlign : public dxfObject
{
  public:

    dxfDimAlign(dxfFloat x1, dxfFloat y1, dxfFloat z1, dxfFloat x2, dxfFloat y2, dxfFloat z2);

    void translate(const translateAttr & t);
    ~dxfDimAlign(void);

    dxfPoint p1;
    dxfPoint p2;


    void dump(const char * prefix = "", ostream & out = cout) const;

};

class dxfDimLinear : public dxfObject
{
  public:

    dxfDimLinear(dxfFloat x1, dxfFloat y1, dxfFloat z1, dxfFloat x2, dxfFloat y2, dxfFloat z2, dxfFloat angle, dxfFloat oblique);
    ~dxfDimLinear(void);

    void translate(const translateAttr & t);
    dxfPoint p1;
    dxfPoint p2;
    dxfFloat 	angle;
    dxfFloat 	oblique;

    void dump(const char * prefix = "", ostream & out = cout) const;
};

class dxfDimRadial : public dxfObject
{
  public:

    dxfDimRadial(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat leader);
    ~dxfDimRadial(void);

    void translate(const translateAttr & t);
    void dump(const char * prefix = "", ostream & out = cout) const;

    dxfPoint center;
    dxfFloat 	leader;
};

class dxfDimDiametric : public dxfObject
{
  public:

    dxfDimDiametric(dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat leader);
    ~dxfDimDiametric(void);

    void translate(const translateAttr & t);
    void dump(const char * prefix = "", ostream & out = cout) const;

    dxfPoint p;
    dxfFloat leader;


};

class dxfDimAngular : public dxfObject
{
  public:

    dxfDimAngular(dxfFloat x1, dxfFloat y1, dxfFloat z1, dxfFloat x2, dxfFloat y2, dxfFloat z2, dxfFloat x3, dxfFloat y3, dxfFloat z3, dxfFloat x4, dxfFloat y4, dxfFloat z4);

    ~dxfDimAngular(void);

    void translate(const translateAttr & t);
    void dump(const char * prefix = "", ostream & out = cout) const;

    dxfPoint p1;
    dxfPoint p2;
    dxfPoint p3;
    dxfPoint p4;

};

class dxfDimAngular3p : public dxfObject
{
  public:


    dxfDimAngular3p(dxfFloat x1, dxfFloat y1, dxfFloat z1, dxfFloat x2, dxfFloat y2, dxfFloat z2, dxfFloat x3, dxfFloat y3, dxfFloat z3);

    ~dxfDimAngular3p(void);

    void translate(const translateAttr & t);
    void dump(const char * prefix = "", ostream & out = cout) const;

    dxfPoint p1;
    dxfPoint p2;
    dxfPoint p3;
};

class dxfLeader : public dxfObject
{
  public:

    dxfLeader(int arrowHeadFlag, int leaderPathType, int leaderCreationFlag, int hooklineDirectionFlag, int hooklineFlag, dxfFloat textAnnotationHeight, dxfFloat textAnnotationWidth);
    ~dxfLeader(void);

    void addVertex(const dxfPoint & p);

    void dump(const char * prefix = "", ostream & out = cout) const;

    void translate(const translateAttr & t);
    int 	arrowHeadFlag;
    int 	leaderPathType;
    int 	leaderCreationFlag;
    int 	hooklineDirectionFlag;
    int 	hooklineFlag;
    dxfFloat 	textAnnotationHeight;
    dxfFloat 	textAnnotationWidth;

    dxfPointVector vertices;


};


class dxfHatch : public dxfObject
{
  public:

    dxfHatch(bool solid, dxfFloat scale, dxfFloat angle, const string & pattern);
    ~dxfHatch(void);

    void addLoop(dxfObjectPtr l);

    void dump(const char * prefix = "", ostream & out = cout) const;

    bool 	  solid;
    dxfFloat 	scale;
    dxfFloat 	angle;
    string 	pattern;

    dxfObjectPtrVector loops;
};

class dxfHatchLoop : public dxfObject
{
  public:

    dxfHatchLoop(void);
    ~dxfHatchLoop(void);

    void addEdge(dxfObjectPtr e);

    void dump(const char * prefix = "", ostream & out = cout) const;

    dxfObjectPtrVector edges;
};

class dxfHatchEdge : public dxfObject
{
  public:

    dxfHatchEdge(int type, bool defined, dxfFloat x1, dxfFloat y1, dxfFloat x2, dxfFloat y2, dxfFloat cx, dxfFloat cy, dxfFloat radius, dxfFloat angle1, dxfFloat angle2, bool ccw);
    ~dxfHatchEdge(void);

    void dump(const char * prefix = "", ostream & out = cout) const;

    void translate(const translateAttr & t);
    int 	type;
    bool 	defined;
    dxfPoint p1;
    dxfPoint p2;
    dxfPoint center;
    dxfFloat 	radius;
    dxfFloat 	angle1;
    dxfFloat 	angle2;
    bool 	ccw;

};

class dxfImage : public dxfObject
{
  public:

    dxfImage(const string & ref, dxfFloat x, dxfFloat y, dxfFloat z, dxfFloat ux, dxfFloat uy, dxfFloat uz, dxfFloat vx, dxfFloat vy, dxfFloat vz, int width, int height, int brightness, int contrast, int fade);

    ~dxfImage(void);

    void translate(const translateAttr & t);
    void dump(const char * prefix = "", ostream & out = cout) const;

    string 	ref;
    dxfPoint insertionPoint;
    dxfPoint u;
    dxfPoint v;
    int 	width;
    int 	height;
    int 	brightness;
    int 	contrast;
    int 	fade;

};

class dxfImageLink : public dxfObject
{
  public:

    dxfImageLink(const string & ref, const string & file);
    ~dxfImageLink(void);

    void dump(const char * prefix = "", ostream & out = cout) const;

    string 	ref;
    string 	file;
};


class dxfVariableVector : public dxfObject
{
  public:

    dxfVariableVector(const string & name, dxfFloat x, dxfFloat y, dxfFloat z, int xxx);
    ~dxfVariableVector(void);

    void dump(const char * prefix = "", ostream & out = cout) const;

    void translate(const translateAttr & t);
    string name;
    dxfPoint p;
    int xxx;
};

class dxfVariableString : public dxfObject
{
  public:


    dxfVariableString(const string & name, const string & value, int xxx);
    ~dxfVariableString(void);

    void dump(const char * prefix = "", ostream & out = cout) const;

    string name;
    string value;
    int xxx;
};

class dxfVariableInt : public dxfObject
{
  public:

    dxfVariableInt(const string & name, int value, int xxx);
    ~dxfVariableInt(void);

    void dump(const char * prefix = "", ostream & out = cout) const;

    string name;
    int value;
    int xxx;
};

class dxfVariableDouble : public dxfObject
{
  public:

    dxfVariableDouble(const string & name, dxfFloat value, int xxx);
    ~dxfVariableDouble(void);

    void dump(const char * prefix = "", ostream & out = cout) const;

    string name;
    dxfFloat value;
    int xxx;



};

#endif
