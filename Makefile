

CXX=g++
DEBUG = -g -Wall
INCLUDE = -Iinclude -I.
LIB = -L. -L./lib 
COMPILE = ${CXX} -c ${DEBUG} ${INCLUDE} ${LIB} -fPIC
LINK = ${CXX} ${LIB} ${DEBUG}


all: codeg

libmapm: lib/libmapm.a

libdxf: lib/libdxf.a

lib/libmapm.a:
	make -C mapm

lib/libdxf.a:
	make -C dxf/src


codeg: codeg.o dxf.o Path.o Layer.o libmapm libdxf 
	${LINK} codeg.o dxf.o Path.o Layer.o -o codeg lib/libmapm.a lib/libdxf.a

codeg.o: codeg.cpp
	${COMPILE} codeg.cpp -o codeg.o

dxf.o: dxf.hpp dxf.cpp
	${COMPILE} dxf.cpp -o dxf.o

Layer.o: Layer.hpp Layer.cpp
	${COMPILE} Layer.cpp -o Layer.o

Path.o: Path.hpp Path.cpp
	${COMPILE} Path.cpp -o Path.o

allclean: clean_all
distclean: clean_all

clean_all:
	rm -Rf *.o codeg
	make -C dxf clean
	make -C mapm clean
	rm -Rf test.png
	rm -Rf gcode.nc test.nc
	rm -Rf gmon.out
	rm -Rf test

clean:
	rm -Rf *.o codeg

.PHONY: libmapm libdxf clean allclean



