#ifndef LAYER_HPP
#define LAYER_HPP

#include "Path.hpp"
#include <vector>

using namespace std;

class Layer
{
  public:

    string     name;
		bool       visible;
    PathVector paths;
};

typedef vector < Layer > LayerVector;


#endif
