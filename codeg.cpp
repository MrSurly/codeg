/*
 * Codeg DXF to GCODE convertor
 * Copyright (C) 2006 Eric Poulsen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 *
 * Author contact info:
 *
 * Eric Poulsen
 * email: eric@zyxod.com
 *   AIM: epoulsen2
 *   ICQ: 1608780
 *
 */

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <getopt.h>
#include <string>

#include <dl_dxf.h>
#include <dl_creationadapter.h>
#include <dl_entities.h>

#include "dxf.hpp"
#include "Path.hpp"
#include "Layer.hpp"

#include <sys/time.h>
#include <time.h>

#include <map>

#define TIME(X) \
	struct timeval TIME_timeval_1; \
struct timeval TIME_timeval_2; \
gettimeofday(&TIME_timeval_1,NULL); \
X \
gettimeofday(&TIME_timeval_2,NULL); \
pretty_interval(TIME_timeval_1,TIME_timeval_2);

#define VERSION_STRING "0.94 Beta"
#define VERSION_NUMERIC ((float)0.94)

#define DESCRIPTION "Codeg: A DXF to GCODE convertor"

#include "license.hpp"

#define USAGE "\
+--------------------------+-----------+------------------------------------+\n\
| Long / Short / Layer     | Parameter | Desc                               |\n\
+--------------------------+-----------+------------------------------------+\n\
| --in                -i   | infile    | Input DXF file                     |\n\
| --out               -o   | outfile   | Output GCODE file                  |\n\
| --layer             -l  *| layername | Opts now apply to named layer      |\n\
| --cw                    *|           | Force path to be clockwise         |\n\
| --ccw                   *|           | Force path to be counter-clockwise |\n\
| --quadrant          -q   | 1|2|3|4   | Quadrant normalization             |\n\
| --max-line-segment  -m  *| float     | Max line segment length for curves |\n\
| --x                 -x   | float     | X offset, inverted for quads 2,3   |\n\
| --y                 -y   | float     | Y offset, inverted for quads 3,4   |\n\
| --safe              -s   | float     | Z safe height, for moving tool     |\n\
| --depth             -d  *| float     | Z cut depth                        |\n\
| --z-feed            -z  *| float     | Z cut feed                         |\n\
| --outside               *|           | Desired result is outside path     |\n\
| --inside                *|           | Desired result is inside path (def)|\n\
| --enable            -E  *|           | Enable layer                       |\n\
| --disable           -D  *|           | Disable layer                      |\n\
| --tool              -t  *| integer   | Tool selection                     |\n\
| --comp              -c  *| [integer] | Use tool radius comp arg is tool # |\n\
| --feed              -f  *| float     | Feed rate                          |\n\
| --term-code         -T   | string    | GCODE termination code             |\n\
| --plot              -P   | plotfile  | Send points to plotfile            |\n\
| --version           -V   |           | Show version                       |\n\
| --warranty          -W   |           | Show warranty                      |\n\
| --license           -L   |           | Show license                       |\n\
| --debug                  |           | Turn on extended debugging         |\n\
| --help              -h   |           | This message                       |\n\
+--------------------------+-----------+------------------------------------+\n\
\n\
codeg is metric agnostic.  It's unaware of inches, millimeters, etc.  It simply treats coordinates as coordinates, and nothing more.\
\n\n\
Options marked with an asterisk (*) are layer-specific.  After specifying a layer with --layer, all the marked options following will apply only to that layer, until another layer is specified.  Any options of this type that are specified before any --layer options will apply to all unspecified layers.\
\n\n\
--quadrant makes sure the resulting cut path is completely within the quadrant.  Paths are also brought up against the origin lines.  For example, with -q 2, the right-most point will be at X coordinate 0 (zero), and the bottom most point will be a Y coordinate 0. A quadrant of zero has no effect on the position.\
\n\n\
--x and --y are relative to the quadrant.  With -q 2, the -x option is inverted before being used. Essentially, when you specify a quadarant, positive values move away from the origin, and negative values move towards it.  With no specified quadrant, these values are used normally -- i.e. negative X values move the object to the left.\
\n\n\
--max-line-segment defaults to 0.01.  Making this number smaller will dramatically increase GCODE size for curves.\
\n\n\
--term-code defaults to M2 (end program).  If you specify a single percent sign (%) as the code, then a percent code is placed alone on the very first and very last line of the output file.\
\n\n\
--tool sets the tool to use\
\n\n\
--comp enables tool radius compensation.  The integer parameter specified is which tools radius to use when compensating.  Note that this value can be different from the --tool value, allowing you to USE one tool while COMPENSATING for a different one.  If no parameter is specified, the tool is assumed to be the same as specified by the --tool option (if any)\
\n\n\
--outside: Normally, the desired resultant piece after a cut is inside the closed path.  This option makes the outside of the path the desired piece.  This only has an effect when using --comp.  --inside is the default.\
\n"


void pretty_interval(struct timeval & t1, struct timeval & t2)
{
	double s1 = (double)t1.tv_sec + ((double)t1.tv_usec/1000000);
	double s2 = (double)t2.tv_sec + ((double)t2.tv_usec/1000000);
	cout  << " elapsed: " << (s2-s1) << endl;

}

using namespace std;

string   arg_in_filename;                 // infile
string   arg_out_filename;                // outfile
string   arg_term_code = "M2";            // gcode term code
int      arg_quadrant = 0;                // quadrant normalization
dxfFloat arg_x_off = 0;                   // x leadin offset adjustment
dxfFloat arg_y_off = 0;                   // x leadin offset adjustment
dxfFloat arg_z_safe_height = 0;           // Height to move tool before G00           
string   arg_plot_filename;               // dump points to stdout

bool     z_axis_enable = false;

typedef map < string, dxfObjectPtr > nameddxfObjectMap;

bool debug = false;

struct LayerOpt
{
	LayerOpt() :
		feed(-1),
		max_line_segment(.01),

		cw(false),
		ccw(false),

		enable(false),
		disable(false),

		comp_tool(-1),
		tool(-1),
		inside(-1),
		cut_depth_set(false),
		cut_depth(0),
		z_feed(-1)
	{}

	void dump(ostream & out = cout)
	{
		out << "|   feed: ";
		if (feed == -1)
			out << "[default]" << endl;
		else
			out << feed << endl;

		out << "| z feed: ";
		if (z_feed == -1)
			out << "[default]" << endl;
		else
			out << z_feed << endl;

		out << "|   comp: ";
		if(comp_tool == -1)
			out << "[default]" << endl;
		else 
			out << comp_tool << endl;
		out << "|   tool: ";
		if(tool == -1)
			out << "[default]" << endl;
		else
			out << tool << endl;
		out << "|    dir: " << (cw ? "CW" : ccw ? "CCW" : "Not specified") << endl;
		out << "|   part: " << (inside ? "inside of path" : "outside of path") << endl;
		if (enable || disable)
			cout << (enable ? "| Enable" : "|Disable");
	
	}

	dxfFloat feed;
	dxfFloat max_line_segment;
	bool     cw;
	bool     ccw;
	bool     enable;  // override 'visible' flag
	bool     disable; // override 'visible' flag
	int      comp_tool;
	int      tool;
	int      inside;
	bool     cut_depth_set;
	dxfFloat cut_depth;
	dxfFloat z_feed;
};

typedef enum { COMP_NONE, COMP_LEFT, COMP_RIGHT } comp_t;

LayerOpt default_layer_opts;
map < string, LayerOpt > layer_opts;




/*
	 class FlatLayer
	 {
	 public:
	 string name;
	 dxfObjectPtrVector objects;
	 };

	 typedef vector < FlatLayer > FlatLayerVector;
	 */


class dxfFilter : public DL_CreationAdapter
{
	public:

		dxfFilter(void) :	doc(new dxfDoc())
		{
			container_chain.push_back(doc);
		}

		void openContainer (dxfObjectPtr v)
		{
			if(debug) cout << "openContainer: " << typeid(*v).name() << endl;
			addObject(v);
			container_chain.push_back(v);
			if(debug) 
			{
				cout << " Current Chain: " << endl;
				dumpChain();
			}
		}

		void closeContainer(void)
		{
			if(debug) cout << "closeContainer: " << typeid(*(container_chain.back())).name() << endl;
			if(container_chain.size() > 1)
				container_chain.pop_back();
			if(debug) 
			{
				cout << " Current Chain: " << endl;
				dumpChain();
			}
		}

		void addObject (dxfObjectPtr p)
		{
			container_chain.back()->push_back(p);
		}

		void dumpChain(void)
		{
			for(dxfObjectPtrVector::const_iterator i = container_chain.begin(); i != container_chain.end(); i++)
				cout<< " " << typeid(**i).name() << endl;
		}

		void dump(const char * prefix = "", ostream & out = cout) const
		{
			doc->dump(prefix,out);
		}


		dxfObjectPtr       doc;
		dxfObjectPtrVector container_chain;

		dxfObjectPtr       current_polyline;
		dxfObjectPtr       current_spline;
		dxfObjectPtr       current_leader;
		dxfObjectPtr       current_hatch;
		dxfObjectPtr       current_hatch_loop;
		dxfObjectPtr       current_block;
		nameddxfObjectMap  blocks_by_name;
		nameddxfObjectMap  layers_by_name;


		string text_accumulator;


		void 	addLayer (const DL_LayerData &item)
		{
			if(debug) cout << "Layer: '" << item.name << "' flags: 0x" << hex << item.flags << dec << endl;
			closeContainer();
			dxfObjectPtr layer (new dxfLayer(item.name,item.flags));
			layers_by_name[item.name] = layer;
			addObject(layer);
		}

		void 	addBlock (const DL_BlockData &item)
		{

			if(debug) cout << "Block: '" << item.name << "' flags: 0x" << hex << item.flags << dec << " " << dxfObject::pretty_coord(item.bpx, item.bpy, item.bpz) <<  endl;
			dxfObjectPtr block(new dxfBlock(item.name,item.flags,item.bpx,item.bpy,item.bpz));
			//openContainer(block);
			blocks_by_name[item.name] = block;
			current_block = block;
		}
		void 	endBlock ()
		{ 
			if(debug) cout << "End Block" << endl; 
			//closeContainer();
			current_block.reset();
		}

		void 	addPoint (const DL_PointData &item)
		{
			if(debug) cout << "Point: " << dxfObject::pretty_coord(item.x,item.y,item.z) << " " << item.layer_name << endl;

			dxfObjectPtr point (new dxfPoint(item.x,item.y,item.z));

			if(layers_by_name.count(item.layer_name) != 0)
				layers_by_name[item.layer_name]->push_back(point);
			if (current_block.get() != NULL)
				current_block->push_back(point);

		}
		void 	addLine (const DL_LineData &item)
		{
			if(debug) cout << "Line: " << dxfObject::pretty_coord(item.x1,item.y1,item.z1) << " to " << dxfObject::pretty_coord(item.x2,item.y2,item.z2) << endl;

			dxfObjectPtr line (new dxfLine(item.x1,item.y1,item.z1,item.x2,item.y2,item.z2));

			if(layers_by_name.count(item.layer_name) != 0)
				layers_by_name[item.layer_name]->push_back(line);
			if (current_block.get() != NULL)
				current_block->push_back(line);

		}
		void 	addArc (const DL_ArcData &item)
		{
			if(debug) cout << "Arc: " << dxfObject::pretty_coord(item.cx,item.cy,item.cz) << " radius: " <<	item.radius << " angle: " << item.angle1 << " to " << item.angle2 << endl;

			dxfObjectPtr arc (new dxfArc(item.cx,item.cy,item.cz,item.radius,item.angle1,item.angle2));

			if(layers_by_name.count(item.layer_name) != 0)
				layers_by_name[item.layer_name]->push_back(arc);

			if (current_block.get() != NULL)
				current_block->push_back(arc);

		}
		void 	addCircle (const DL_CircleData &item)
		{
			if(debug) cout << "Circle: " << dxfObject::pretty_coord(item.cx, item.cy, item.cz) << " radius: " << item.radius << endl;

			dxfObjectPtr circle (new dxfCircle(item.cx,item.cy,item.cz,item.radius));

			if(layers_by_name.count(item.layer_name) != 0)
				layers_by_name[item.layer_name]->push_back(circle);

			if (current_block.get() != NULL)
				current_block->push_back(circle);

		}
		void 	addEllipse (const DL_EllipseData &item)
		{
			if(debug) cout << "Ellipse: " << dxfObject::pretty_coord(item.cx,item.cy,item.cz) << " / " << dxfObject::pretty_coord(item.mx,item.my,item.mz) << " ratio: " << item.ratio << " angle: " << item.angle1 << " to " << item.angle2 << endl;

			dxfObjectPtr ellipse (new dxfEllipse(item.cx,item.cy,item.cz,item.mx,item.my,item.mz,item.ratio,dxfObject::rad2deg(item.angle1),dxfObject::rad2deg(item.angle2)));

			if(layers_by_name.count(item.layer_name) != 0)
				layers_by_name[item.layer_name]->push_back(ellipse);

			if (current_block.get() != NULL)
				current_block->push_back(ellipse);


		}
		void 	addPolyline (const DL_PolylineData &item)
		{
			if(debug) cout << "Polyline: flags: 0x" << hex << item.flags << dec << " m: " << item.m << " n: " << item.n << " number: " << item.number << endl;

			dxfObjectPtr polyline (new dxfPolyline(item.flags));

			if(layers_by_name.count(item.layer_name) != 0)
				layers_by_name[item.layer_name]->push_back(polyline);

			if (current_block.get() != NULL)
				current_block->push_back(polyline);

			current_polyline = polyline;
		}

		void 	addVertex (const DL_VertexData &item)
		{
			if(debug) cout << "Polyline Vertex: " << dxfObject::pretty_coord(item.x,item.y,item.z) << " bulge: " << item.bulge << endl;
			if (current_polyline.get() != NULL)
				((dxfPolyline *)current_polyline.get())->addVertex(dxfPolylineVertex(item.x,item.y,item.z,item.bulge));
		}
		void 	addSpline (const DL_SplineData &item)
		{
			if(debug) cout << "Spline: flags: 0x" << hex << item.flags << dec << " degree: " << item.degree << " nKnots: " << item.nKnots << " nControl: " << item.nControl << endl;

			dxfObjectPtr spline (new dxfSpline(item.flags,item.degree));

			if(layers_by_name.count(item.layer_name) != 0)
				layers_by_name[item.layer_name]->push_back(spline);

			if (current_block.get() != NULL)
				current_block->push_back(spline);

			current_spline = spline;
		}
		void 	addControlPoint (const DL_ControlPointData &item)
		{
			if(debug) cout << "Spline Control Point: " << dxfObject::pretty_coord(item.x,item.y,item.z) << endl;
			if (current_spline.get() != NULL)
				((dxfSpline *)current_spline.get())->addPoint(dxfPoint(item.x,item.y,item.z));
		}
		void 	addKnot (const DL_KnotData &item)
		{
			if(debug) cout << "Spline Knot: k: " << item.k << endl;
			if (current_spline.get() != NULL)
				((dxfSpline *)current_spline.get())->addKnot(item.k);
		}
		void 	addInsert (const DL_InsertData &item)
		{
			/*
				 cout << "Insert: " <<
				 "name: '" << item.name << "'" <<
				 " " << dxfObject::pretty_coord(item.ipx,item.ipy,item.ipz) <<
				 " angle: " << item.angle <<
				 " cols: " << item.cols <<
				 " colSp: " << item.colSp <<
				 " rows: " << item.rows <<
				 " rowSp: " << item.rowSp <<
				 " scale (x,y,z):  " << dxfObject::pretty_coord(item.sx, item.sy, item.sz) << 
				 endl;
				 */

			//addObject((dxfObjectPtr(new dxfInsert(item.name,item.ipx,item.ipy,item.ipz,item.sx,item.sy,item.sz,dxfObject::deg2rad(item.angle), item.cols, item.rows,item.colSp,item.rowSp))));

			if (blocks_by_name.count(item.name) != 0)
			{
				translateAttr t;
				t.offset = dxfPoint(item.ipx,item.ipy,item.ipz);
				if(debug) cout << "Translating block '" << item.name << "' to point " << dxfObject::pretty_coord(t.offset) << endl;
				blocks_by_name[item.name]->translate(t);
			}

		}
		void 	addMText (const DL_MTextData &item)
		{
			if(debug) cout << "MultiText: " << dxfObject::pretty_coord(item.ipx, item.ipy, item.ipz) << "'" << item.text << "'" << " style: '" << item.style << "'" << " width: " << item.width << " height: " << item.height << " att point: " << item.attachmentPoint << " draw dir: " << item.drawingDirection << " line style: " << item.lineSpacingStyle << " line factor: " << item.lineSpacingFactor <<	" angle: " << item.angle <<	endl;

			addObject(dxfObjectPtr(new dxfMText(item.ipx,item.ipy,item.ipz,item.width,item.height,item.attachmentPoint,item.drawingDirection,item.lineSpacingStyle,item.lineSpacingFactor, text_accumulator, item.style,item.angle))); 

			text_accumulator = "";
		}

		void 	addMTextChunk (const char * x)
		{
			if(debug) cout << "MultiText Chunk: '" << x << "'" << endl;
			text_accumulator += x;
		}

		void 	addText (const DL_TextData &item)
		{
			if(debug) cout << "Text: " << "'" << item.text << "'" << " style: '" << item.style << "'" << " angle: " << item.angle << " height: " << item.height << " x scale: " << item.xScaleFactor << " vert just: " << item.vJustification << " horiz just: " << item.hJustification << " flags: 0x" << hex << item.textGenerationFlags << dec << " align point: " << dxfObject::pretty_coord(item.apx,item.apy,item.apz) << " insert point: " << dxfObject::pretty_coord(item.ipx,item.ipy,item.ipz) << endl;

			addObject(dxfObjectPtr(new dxfText(item.ipx,item.ipy,item.ipz,item.apx,item.apy,item.apz,item.height,item.xScaleFactor,item.textGenerationFlags,item.hJustification,item.vJustification,item.text,item.style,item.angle))); 
		}
		void 	addDimAlign (const DL_DimensionData &item, const DL_DimAlignedData &y)
		{
			if(debug) cout << "DimAlign " << endl;
			dxfDimAlign * q = new dxfDimAlign(y.epx1,y.epy1,y.epz1,y.epx2,y.epy2,y.epz2);
			addObject((dxfObjectPtr(new dxfDim( item.dpx, item.dpy, item.dpz, item.mpx, item.mpy, item.mpz, item.type, item.attachmentPoint, item.lineSpacingStyle, item.lineSpacingFactor, item.text, item.style, item.angle, q))));
		}
		void 	addDimLinear (const DL_DimensionData &item, const DL_DimLinearData &y)
		{
			if(debug) cout << "DimLinear " << endl;
			dxfDimLinear * q = new dxfDimLinear( y.dpx1, y.dpy1, y.dpz1, y.dpx2, y.dpy2, y.dpz2, y.angle, y.oblique);
			addObject((dxfObjectPtr(new dxfDim( item.dpx, item.dpy, item.dpz, item.mpx, item.mpy, item.mpz, item.type, item.attachmentPoint, item.lineSpacingStyle, item.lineSpacingFactor, item.text, item.style, item.angle, q))));
		}

		void 	addDimRadial (const DL_DimensionData &item, const DL_DimRadialData &y)
		{
			if(debug) cout << "DimRadial " << endl;
			dxfDimRadial * q = new dxfDimRadial(y.dpx,y.dpy,y.dpz,y.leader);
			addObject((dxfObjectPtr(new dxfDim( item.dpx, item.dpy, item.dpz, item.mpx, item.mpy, item.mpz, item.type, item.attachmentPoint, item.lineSpacingStyle, item.lineSpacingFactor, item.text, item.style, item.angle, q))));
		}
		void 	addDimDiametric (const DL_DimensionData &item, const DL_DimDiametricData &y)
		{
			if(debug) cout << "DimDiametric " << endl;
			dxfDimDiametric * q = new dxfDimDiametric(y.dpx,y.dpy,y.dpz,y.leader);
			addObject((dxfObjectPtr(new dxfDim( item.dpx, item.dpy, item.dpz, item.mpx, item.mpy, item.mpz, item.type, item.attachmentPoint, item.lineSpacingStyle, item.lineSpacingFactor, item.text, item.style, item.angle, q))));
		}
		void 	addDimAngular (const DL_DimensionData &item, const DL_DimAngularData &y)
		{
			if(debug) cout << "DimAngular " << endl;
			dxfDimAngular * q = new dxfDimAngular(y.dpx1, y.dpy1, y.dpz1, y.dpx2, y.dpy2, y.dpz2, y.dpx3, y.dpy3, y.dpz3, y.dpx4, y.dpy4, y.dpz4);
			addObject((dxfObjectPtr(new dxfDim( item.dpx, item.dpy, item.dpz, item.mpx, item.mpy, item.mpz, item.type, item.attachmentPoint, item.lineSpacingStyle, item.lineSpacingFactor, item.text, item.style, item.angle, q))));
		}
		void 	addDimAngular3P (const DL_DimensionData &item, const DL_DimAngular3PData &y)
		{
			if(debug) cout << "DimAngular3P " << endl;
			dxfDimAngular3p * q = new dxfDimAngular3p(y.dpx1, y.dpy1, y.dpz1, y.dpx2, y.dpy2, y.dpz2, y.dpx3, y.dpy3, y.dpz3);
			addObject((dxfObjectPtr(new dxfDim( item.dpx, item.dpy, item.dpz, item.mpx, item.mpy, item.mpz, item.type, item.attachmentPoint, item.lineSpacingStyle, item.lineSpacingFactor, item.text, item.style, item.angle, q))));
		}
		void 	addLeader (const DL_LeaderData &item)
		{
			if(debug) cout << "Leader " << endl;
			current_leader = dxfObjectPtr(new dxfLeader(item.arrowHeadFlag, item.leaderPathType, item.leaderCreationFlag, item.hooklineDirectionFlag, item.hooklineFlag, item.textAnnotationHeight, item.textAnnotationWidth));

			addObject((current_leader));
		}

		void 	addLeaderVertex (const DL_LeaderVertexData &item)
		{
			if(debug) cout << "LeaderVertex" << endl;
			if (current_leader.get() != NULL)
				((dxfLeader *)current_leader.get())->addVertex(dxfPoint(item.x,item.y,item.z));
		}

		void 	addHatch (const DL_HatchData &item)
		{
			if(debug) cout << "Hatch" << endl;
			current_hatch = dxfObjectPtr(new dxfHatch(item.solid,item.scale,item.angle,item.pattern));
			addObject((current_hatch));
		}

		void 	addHatchLoop (const DL_HatchLoopData &item)
		{
			if(debug) cout << "HatchLoopData" << endl;
			if (current_hatch.get() != NULL)
			{
				current_hatch_loop = dxfObjectPtr(new dxfHatchLoop());
				((dxfHatch *)current_hatch.get())->addLoop(current_hatch_loop);
			}
		}

		void 	addHatchEdge (const DL_HatchEdgeData &item)
		{
			if(debug) cout << "HatchEdge" << endl;
			if(current_hatch_loop.get() != NULL)
			{
				((dxfHatchLoop *)current_hatch_loop.get())->addEdge(dxfObjectPtr(new dxfHatchEdge(item.type,item.defined,item.x1,item.y1,item.x2,item.y2,item.cx,item.cy,item.radius,item.angle1,item.angle2,item.ccw)));
			}
		}

		void 	addImage (const DL_ImageData &item)
		{
			if(debug) cout << "Image" << endl;
			addObject((dxfObjectPtr(new dxfImage(item.ref,item.ipx,item.ipy,item.ipz,item.ux,item.uy,item.uz,item.vx,item.vy,item.vz,item.width,item.height,item.brightness,item.contrast,item.fade))));
		}

		void 	linkImage (const DL_ImageDefData &item)
		{
			if(debug) cout << "linkImage" << endl;
			addObject((dxfObjectPtr(new dxfImageLink(item.ref,item.file))));
		}


		void 	setVariableVector (const char *name , double x, double y , double z , int xxx)
		{
			if(debug) cout << "VariableVector: '" << name << "' "<< dxfObject::pretty_coord(x,y,z) << endl;
			addObject(dxfObjectPtr(new dxfVariableVector(name,x,y,z,xxx)));
		}
		void 	setVariableString (const char *name , const char *value, int xxx)
		{
			if(debug) cout << "VariableString: '" << name << "' '" << value <<"'" << endl;
			addObject(dxfObjectPtr(new dxfVariableString(name,value,xxx)));
		}
		void 	setVariableInt (const char *name, int value, int xxx)
		{
			if(debug) cout << "VariableInt: '" << name << "' " << value << endl;
			addObject(dxfObjectPtr(new dxfVariableInt(name,value,xxx)));
		}
		void 	setVariableDouble (const char *name, double value, int xxx)
		{
			if(debug) cout << "VariableDouble: " << name << "' " << value << endl;
			addObject(dxfObjectPtr(new dxfVariableDouble(name,value,xxx)));
		}
		void 	endSequence ()
		{
			if(debug) cout  << __FILE__ << ":" << __LINE__ << " endSequence";
			if (current_polyline.get() != NULL)
			{
				current_polyline.reset();
				if(debug) cout << " [CLOSE POLYLINE]";
			}
			if(debug) cout << endl;
		}

		void 	endEntity ()
		{
			if(debug) cout << __FILE__ << ":" << __LINE__ << " endEntity";
			if (current_polyline.get() != NULL)
			{
				current_polyline.reset();
				if(debug) cout << " [CLOSE POLYLINE]";
			}
			cout << endl;
		}
};

void about(void)
{
	cout << DESCRIPTION << endl << "Version " << VERSION_STRING << endl;
	cout << "Copyright 2005-2017 Eric Poulsen. Released under the GPL." << endl ;
	if (debug)
		cout << "Extended debugging enabled " << endl;
}

void usage(void)
{
	cout << USAGE << endl;
}

void warranty(void)
{
	cout << WARRANTY << endl;
}

void license(void)
{
	cout << LICENSE << endl;
}

void get_args(int argc, char ** argv)
{
	static struct option long_options[] =
	{
		{"outside",0,0,0},
		{"inside",0,0,0},
		{"cw",0,0,0},
		{"ccw",0,0,0},
		{"debug",0,0,0},
		{"in",1,0,'i'},
		{"out",1,0,'o'},
		{"enable",0,0,'E'},
		{"disable",0,0,'D'},
		{"quadrant",1,0,'q'},
		{"layer",1,0,'l'},
		{"max-line-segment",1,0,'m'},
		{"x",1,0,'x'},
		{"y",1,0,'y'},
		{"safe",1,0,'s'},
		{"depth",1,0,'d'},
		{"zfeed",1,0,'z'},
		{"z-feed",1,0,'z'},
		{"comp",2,0,'c'},
		{"tool",1,0,'t'},
		{"feed",1,0,'f'},
		{"term-code",1,0,'T'},
		{"plot",1,0,'P'},
		{"version",0,0,'V'},
		{"warranty",0,0,'W'},
		{"license",0,0,'L'},
		{"licence",0,0,'L'},
		{"help",0,0,'h'},
		{"usage",0,0,'h'},
		{0,0,0,0}
	};

	default_layer_opts.feed = 1;
	default_layer_opts.z_feed = 1;
	default_layer_opts.max_line_segment = 0.01;
	default_layer_opts.inside = true;

	int c = 0;
	int option_index = 0;
	LayerOpt * current_layer_opts = &default_layer_opts;

	bool unspecified_comp = false;

	while (1)
	{
		c = getopt_long(argc,argv,"T:i:o:q:m:x:y:f:t:c::l:P:z:s:d:VWLhED",long_options,&option_index);

		if (c == -1)
			break;

		switch (c)
		{
			case 0: // Long option
				switch(option_index)
				{
					case 0: // outside
						current_layer_opts->inside = false;
						break;

					case 1: // inside
						current_layer_opts->inside = true;
						break;

					case 2: // cw
						if (current_layer_opts->ccw)
						{
							cout << "ERROR: Cannot specify both --cw and --ccw for one layer.  Terminating." << endl << endl;
							exit(1);
						}
						current_layer_opts->cw = true;
						break;

					case 3: // ccw
						if (current_layer_opts->cw)
						{
							cout << "ERROR: Cannot specify both --cw and --ccw for one layer.  Terminating." << endl << endl;
							exit(1);
						}
						current_layer_opts->ccw = true;
						break;
					case 4: // debug
						debug = true;
						break;
				}
				break;

			case 'E': // --enable
				if (current_layer_opts->disable)
				{
					cout << "ERROR: Cannot specify both --enable and --disable for one layer.  Terminating." << endl << endl;
					exit(1);
				}
				current_layer_opts->enable = true;
				break;

			case 'D': // --disable
				if (current_layer_opts->enable)
				{
					cout << "ERROR: Cannot specify both --enable and --disable for one layer.  Terminating." << endl << endl;
					exit(1);
				}
				current_layer_opts->disable = true;
				break;

			case 'l':  // -- layer
				current_layer_opts = &(layer_opts[optarg]);
				unspecified_comp = false;
				break;


			case 'i': // --in
				arg_in_filename = optarg;
				break;

			case 'o': // --out
				arg_out_filename = optarg;
				break;

			case 'q': // --quadrant
				arg_quadrant = atoi(optarg);
				if(arg_quadrant > 4 || arg_quadrant < 1)
				{
					cerr << "ERROR: --quadrant must be an integer value from 1 to 4" << endl;
					exit(1);
				}
				break;

			case 'm': // --max-line-segment
				current_layer_opts->max_line_segment = atof(optarg);
				break;

			case 'x': // --x
				arg_x_off = dxfFloat(optarg);
				break;

			case 'y': // --y
				arg_y_off = dxfFloat(optarg);
				break;

			case 's': // --safe
				arg_z_safe_height = dxfFloat(optarg);
				z_axis_enable = true;
				break;

			case 'd': // --depth
				current_layer_opts->cut_depth = optarg;
				current_layer_opts->cut_depth_set = true;
				z_axis_enable = true;
				break;

			case 'z': // --zfeed
				current_layer_opts->z_feed = optarg;
				z_axis_enable = true;
				break;


			case 'f': // --feed
				{
					dxfFloat f(optarg);
					if (f <= 0)
						cerr << "WARNING: Feed cannot be <= 0, ignoring" << endl;
					else
						current_layer_opts->feed = f;
				}
				break;

			case 'T': // --term-code
				arg_term_code = optarg;
				break;

			case 'P': // --plot
				arg_plot_filename = optarg;
				break;

			case 'W': // --warranty
			case 'V': // --version
				cout << endl;
				about();
				cout << endl; 
				warranty();
				cout << endl << "Use the --license option for license information." << endl << endl;
				exit(0);
				break;

			case 'L': // -- license
				cout << endl;
				about();
				cout << endl;
				warranty();
				cout << endl;
				license();
				cout << endl;
				exit(0);
				break;

			case 'h': // --help
				cout << endl;
				usage();
				cout << endl;
				exit(0);
				break;

			case 'c': // --comp
				cout << "optarg: " << (optarg == NULL ? "[nil]" : optarg) <<endl;
				if (optarg != NULL)
					current_layer_opts->comp_tool = atoi(optarg); // explicitly set tool
				else
				{	
					if (current_layer_opts->tool != -1)
						current_layer_opts->comp_tool = current_layer_opts->tool; // Set to previously set tool option
					unspecified_comp = true; // Set comp to not-yet-seen tool
				}
				break;

			case 't': // --tool
				current_layer_opts->tool = atoi(optarg);
				if (unspecified_comp)
				{
					(current_layer_opts->comp_tool) = (current_layer_opts->tool);
					unspecified_comp = false;
				}
				break;

		}
	}
}


void flatten (dxfObjectPtrVector tree, dxfObjectPtrVector & target, int level = 0 )
{
	for (dxfObjectPtrVector::const_iterator i = tree.begin(); i != tree.end(); i++)
	{
		if (typeid(**i) == typeid(dxfLayer))
		{
			dxfLayer * layer = (dxfLayer *)(i->get());
			if (!layer->objects.empty())
			{
				target.push_back(dxfObjectPtr(new dxfLayer(layer->name,layer->flags)));
				flatten (layer->objects,target,level+1);
			}
		}
		else if (typeid(**i) == typeid(dxfBlock))
		{
			dxfBlock * block = (dxfBlock *)(i->get());
			if (!block->objects.empty())
				flatten (block->objects,target,level+1);
		}
		else if ((*i)->isDrawable())
			((dxfLayer *)target.back().get())->objects.push_back(*i);
	}
}

void chain (dxfObjectPtrVector & source, dxfObjectPtrVector & out )
{
	out.clear();
	out.push_back(source.back());
	source.pop_back();

	bool item_added;
	do
	{
		dxfObjectPtr last = out.back();
		dxfObjectPtr first = out.front();
		dxfPoint endPoint = last->endPoint();
		dxfPoint beginPoint = first->beginPoint();
		item_added = false;
		//cout << "Items remaining in source: " << source.size() << endl;
		//endPoint.dump("Current Point: ");
		for(dxfObjectPtrVector::iterator i = source.begin(); i != source.end(); i++)
		{
			dxfPoint b = (*i)->beginPoint();
			dxfPoint e = (*i)->endPoint();
			if (endPoint == b)
			{
				out.push_back(*i);
				item_added = true;
				source.erase(i);
				break;
			}
			else if (endPoint == e)
			{
				(*i)->reverse();
				out.push_back(*i);
				source.erase(i);
				item_added = true;
				break;
			}
			else if (beginPoint == b)
			{
				(*i)->reverse();
				out.insert(out.begin(),*i);
				source.erase(i);
				item_added = true;
				break;
			}
			else if (beginPoint == e)
			{
				out.insert(out.begin(),*i);
				source.erase(i);
				item_added = true;
				break;
			}
		}
	} while(item_added && !source.empty());
}



int main (int argc, char **argv)
{
	// Get command line arguments

	get_args(argc, argv);

	about();
	cout << endl;

	cout << "Infile: " << arg_in_filename << endl;
	cout << "Outfile: " << arg_out_filename << endl;


	DL_Dxf dxf_reader;
	dxfFilter dxf_filter;

	// Use libdxf to read the dxf file

	dxf_reader.in(arg_in_filename,&dxf_filter);

	// Dump out document

	/*
	cout << endl << endl << "DOCDUMP" << endl<<endl;
	dxf_filter.dump("");
	*/

	// Flatten all the entities per layer.  Items are now only separated by layers.
	// Blocks and other containers are discarded

	dxfObjectPtrVector dxfLayers; // Holds flattened layers
	flatten(((dxfDoc *)dxf_filter.doc.get())->objects,dxfLayers);
	cout << dxfLayers.size() << " Layers" << endl;


	LayerVector layers;


	// Iterate all layers, chaining objects and generating paths from object chains
	translateAttr offset_translate;
	translateAttr default_translate;

	cout << "Chaining and rendering paths ...";
	cout.flush();
	for(dxfObjectPtrVector::iterator i = dxfLayers.begin(); i != dxfLayers.end(); i++)
	{
		dxfLayer * source_layer = (dxfLayer *)(i->get());
    //cout << "Layer: " << source_layer->name << endl; /* DEBUG */

		dxfObjectPtrVector layer_objects = source_layer->objects;

		layers.push_back(Layer());
		Layer & layer = layers.back();
		layer.visible = source_layer->visible;
		layer.name = source_layer->name;
		while(!layer_objects.empty())
		{
      // cout << "Layer Object Loop" << endl; /* DEBUG */
			layer.paths.push_back(Path());
			Path & path = layer.paths.back();
			dxfObjectPtrVector out;
			chain(layer_objects,out);

			path.closed = out.front()->beginPoint() == out.back()->endPoint(); 


			bool first = true;
			default_translate.max_line_seg = layer_opts.count(source_layer->name) != 0 ? 
				layer_opts[source_layer->name].max_line_segment : 
				default_layer_opts.max_line_segment;

			for(dxfObjectPtrVector::const_iterator x = out.begin(); x != out.end(); x++)
			{
				//  cout << "out loop" << endl; /* DEBUG */
				//  cout << "Object Type: "<< typeid(**x).name() << endl;
 				//  cout << " Line Seg: " << default_translate.max_line_seg << endl;
				(*x)->toPoints(path.points(),default_translate,first && !path.closed);
				first = false;
			}
		}
	}
	cout << " done." << endl;

	//
	// Find bounds of all layers
	//


	bool reset = true;
	dxfPoint lower_left, upper_right, left, right, top, bottom;
	// left, right, top and bottom are actual points on a path.

	for(LayerVector::iterator layer = layers.begin(); layer != layers.end(); layer++)
	{
		//cout << "Bounds search of layer '" << layer->name << "'" << endl;
		for(PathVector::iterator path = layer->paths.begin(); path != layer->paths.end(); path++)
		{
			path->findBounds(lower_left,upper_right, left, right, top, bottom,reset);
			reset = false;
		}
	}

	dxfFloat left_bound = lower_left.x;
	dxfFloat right_bound = upper_right.x;
	dxfFloat bottom_bound = lower_left.y;
	dxfFloat top_bound = upper_right.y;
	dxfFloat width = right_bound - left_bound;
	dxfFloat height = top_bound - bottom_bound;

	dxfFloat x_offset = 0;
	dxfFloat y_offset = 0;

	switch(arg_quadrant)
	{
		case 0:
			// use x lead and y lead as offsets
			x_offset = arg_x_off;
			y_offset = arg_y_off;
			break;

		case 1:
			x_offset = -left_bound + arg_x_off;
			y_offset = -bottom_bound + arg_y_off;
			break;

		case 2:
			x_offset = -right_bound - arg_x_off;
			y_offset = -bottom_bound + arg_y_off;
			break;

		case 3:
			x_offset = -right_bound - arg_x_off;
			y_offset = -top_bound - arg_y_off;
			break;

		case 4:
			x_offset = -left_bound + arg_x_off;
			y_offset = -top_bound - arg_y_off;
			break;
	}

	cout << "      quadrant: ";
	if (arg_quadrant != 0)
		cout << arg_quadrant << endl;
	else
		cout << "Not specified" << endl;
	
	cout << "          left: " << left_bound << endl;
	cout << "         right: " << right_bound << endl;
	cout << "           top: " << top_bound << endl;
	cout << "        bottom: " << bottom_bound << endl;
	cout << "         width: " << width << endl;
	cout << "        height: " << height << endl;
	cout << "  req X offset: " << arg_x_off << endl;
	cout << "  req Y offset: " << arg_y_off << endl;
	cout << " real X offset: " << x_offset << endl;
	cout << " real Y offset: " << y_offset << endl;

	offset_translate.offset = dxfPoint(x_offset,y_offset,0);

	// Open output file

	filebuf fb_out;
	if(fb_out.open (arg_out_filename.c_str(),ios::out) == NULL)
	{
		cerr << "Unable to open output file '" << arg_out_filename << "' for writing" << endl;
		exit(1);
	}
	ostream os(&fb_out);

	// Open plot file

	filebuf fb_plot;
	if (arg_plot_filename != "")
	{
		if(fb_plot.open (arg_plot_filename.c_str(),ios::out) == NULL)
		{
			cerr << "Unable to open plot file '" << arg_plot_filename << "' for writing" << endl;
			exit(1);
		}
	}
	ostream os_plot(&fb_plot);

	// Final output
	//
	// Iterate through layers and generate GCODE
	//
	//
	
	if (arg_term_code == "%")
		os << "%" << endl;


	LayerOpt * options = &default_layer_opts;

	comp_t   current_comp_dir = COMP_NONE;
	int      current_comp_tool = 0;
	int      current_tool = -1;
	dxfFloat current_feed = -1;
	int      line_num = 0;
	dxfFloat current_z = 0;
	dxfPoint origin(0,0,0);
	dxfPoint cursor = origin;

	cursor.dump("BEGIN: ",os_plot);
	
	cout << endl << "Default Options:" << endl;
	options->dump();
	cout << endl;
	cout << "Z axis: " << (z_axis_enable ? "Enabled" : "Disabled") << endl << endl;

	// Go to safe height
	

	/******************************/
	/*                            */
	/* MAIN LAYER LOOP            */
	/*                            */
	/******************************/

	for(LayerVector::iterator layer = layers.begin(); layer != layers.end(); layer++)
	{

		bool opt_set = false;
		ostringstream opt_string;

		//
		// Find layer-specific options, if any
		//

		if(layer_opts.count(layer->name) != 0)
			options = &(layer_opts[layer->name]);
		else
			options = &default_layer_opts;

		cout <<endl<< "LAYER: '" << layer->name << "' Visible: " << (layer->visible ? "Yes" : "No") ;

		//
		// Enable hidden layers, or disable visible layers, if specified
		//

		bool enable  = options->enable;
		bool disable = options->disable;

		if (!enable && !disable) // If layer isn't specific about enable/disable, use default opts
		{
			enable  = default_layer_opts.enable;
			disable = default_layer_opts.disable;
		}

		if (disable)
		{
				cout << (layer->visible ? ", but Disabled, skipping" : "" ) << endl;
				continue;
		}

		if (!layer->visible)
		{
			if (enable)
				cout << ", but Enabled flag is set" << endl;
			else
			{
				cout << endl;
				continue;
			}
		}
		else
			cout << endl;

		if (options == &default_layer_opts)
			cout << "Using Default Options" << endl;
		else
		{
			cout << "Using Layer Options:" << endl;
			options->dump();
		}



		// Determine tool

		int _tool = options->tool != -1 ? options->tool : default_layer_opts.tool;

		if (_tool != current_tool)
		{
			opt_string << (opt_set ? " " : "") << "T"<< _tool << " M6";
			current_tool = _tool;
			opt_set = true;
		}

		/******************************/
		/*                            */
		/* MAIN PATH LOOP             */
		/*                            */
		/******************************/

		for(PathVector::iterator path = layer->paths.begin(); path != layer->paths.end(); path++)
		{
			bool clockwise = path->clockwise();

			bool cw, ccw;

			cw = options->cw;
			ccw = options->ccw;

			if (!cw && !ccw)
			{
				cw = default_layer_opts.cw;
				ccw = default_layer_opts.ccw;
			}
			if ((clockwise && options->ccw) || (!clockwise && options->cw))
			{
				path->reverse();
				clockwise = !clockwise;
			}


			path->translate(offset_translate);

			int _comp_tool = options->comp_tool != -1 ? options->comp_tool : default_layer_opts.comp_tool;
			bool _inside = options->inside != -1 ? options->inside : default_layer_opts.inside;
			comp_t _comp_dir = _comp_tool > 0 ? clockwise == _inside ? COMP_LEFT : COMP_RIGHT : COMP_NONE;
			if (debug)
				cout << " Path: " << (clockwise ? "CW" : "CCW") << (options->cw || options->ccw ? " (Forced)" : "") << (_inside ? " Inside" : " Outside") << endl ;

			if (_comp_dir != current_comp_dir || _comp_tool != current_comp_tool)
			{
				opt_string << (opt_set ? " " : "");
				switch(_comp_dir)
				{
					case COMP_NONE:
						opt_string << "G40";
						break;
					case COMP_LEFT:
						opt_string << "G41 D"<< _comp_tool;
						break;
					case COMP_RIGHT:
						opt_string << "G42 D"<< _comp_tool;
						break;
				}
				opt_set = true;
				current_comp_dir  = _comp_dir;
				current_comp_tool = _comp_tool;

			}
			if (debug)
			{
				cout << " Searching for point nearest to " << dxfObject::pretty_coord(cursor) << " out of " << path->const_points().size()+1 << " segments: ";
				cout.flush();
			}
			dxfPointVector::const_iterator start_point = path->findNearestPoint(cursor,.05);

			if(debug)
				cout << dxfObject::pretty_coord(*start_point) << endl;

			dxfFloat _feed;

			// >> Z axis safe, rapid position, plunge
			if (z_axis_enable)
			{
				// Move Z axis to safe height
				dxfObject::printGcodeLine(os,line_num);
				os << "G00 Z" << arg_z_safe_height << endl;
				os_plot << "LIFT: " << arg_z_safe_height << endl;

				// Move to start point

				dxfObject::printGcodeLine(os,line_num);
				os << "G00 X" << start_point->x << " Y" << start_point->y << endl;
				os_plot << "RAPID: " << dxfObject::pretty_coord(start_point->x,start_point->y,arg_z_safe_height) << endl;
				cursor = *start_point;

				//start_point->dump(" ");

				// do plunge
				

				_feed = options->z_feed != -1 ? options->z_feed : default_layer_opts.z_feed;
				dxfFloat _cut_depth = options->cut_depth_set ? options->cut_depth : default_layer_opts.cut_depth;

				if (_feed != current_feed)
				{
					opt_string << (opt_set ? " " : "");
					opt_string << "F" << _feed;
					current_feed = _feed;
					opt_set = true;
				}

				if (opt_set)
				{
					dxfObject::printGcodeLine(os,line_num);
					os << opt_string.str() << endl;
					opt_set = false;
					opt_string.str("");
				}

				if (_cut_depth != arg_z_safe_height)
				{
					dxfObject::printGcodeLine(os,line_num);
					os << "G01 Z" << _cut_depth;
					os << endl;
				}

				os_plot << "PLUNGE: " << _cut_depth << endl;
			}
			// << Z axis safe, rapid position, plunge

			// Determine feed

			_feed = options->feed != -1 ? options->feed : default_layer_opts.feed;
			if (_feed != current_feed)
			{
				opt_string << (opt_set ? " " : "") << "F" << _feed;
				current_feed = _feed;
				opt_set = true;
			}

			if (opt_set)
			{
				dxfObject::printGcodeLine(os,line_num);
				os << opt_string.str() << endl;
				opt_set = false;
				opt_string.str("");
			}
			if(debug)
				path->dump();
			if (debug) 
			{
				cout << " cursor: "      << dxfObject::pretty_coord(cursor) ;
				cout << " start point: " <<  dxfObject::pretty_coord(*start_point) << endl;
			}


			cursor = path->toGcode(os, line_num, default_translate, start_point,cursor != (*start_point),os_plot);
			//cursor = path->toGcode(os, line_num, default_translate,true,os_plot);
		} // End of path loop
	} // End of main layer loop

	if (z_axis_enable)
	{
		// Move Z axis to safe height
		dxfObject::printGcodeLine(os,line_num);
		os << "G00 Z" << arg_z_safe_height << endl;
		os_plot << "LIFT: " << arg_z_safe_height << endl;

		// Move to start point

		dxfObject::printGcodeLine(os,line_num);
		os << "G00 X" << origin.x << " Y" << origin.y << endl;
		os_plot << "RAPID: " << dxfObject::pretty_coord(origin.x,origin.y,arg_z_safe_height) << endl;
	}
	else
	{
		// Move to start point

		dxfObject::printGcodeLine(os,line_num);
		os << "G01 X" << origin.x << " Y" << origin.y << endl;
		os_plot << "CUT: " << dxfObject::pretty_coord(origin.x,origin.y,0) << endl;
	}

	if (arg_term_code != "%")
		dxfObject::printGcodeLine(os,line_num);

	os << arg_term_code << endl;
	fb_out.close();
	fb_plot.close();

}
