//: C20:Ring.cpp
// Making a "ring" data structure from the STL

#include <iostream>
#include <list>
#include <string>

using namespace std;

template<class T>
class ring 
{
	list< T > lst;
	public:
	// Declaration necessary so the following 
	// 'friend' statement sees this 'iterator' 
	// instead of std::iterator:
	class iterator;
	friend class iterator;
	class iterator : public std::iterator< std::bidirectional_iterator_tag, T ,ptrdiff_t >
	{
		list< T >* r;
		typename list< T >::iterator it;

		public:

		// "typename" necessary to resolve nesting:

		iterator(list<T>& lst, const typename list<T>::iterator& i) : r(&lst), it(i) {}

		bool operator==(const iterator& x) const { return it == x.it; }
		bool operator!=(const iterator& x) const { return !(*this == x); }
		typename list<T>::reference operator*(void) const { return *it; }
		iterator& operator++(void) { ++it; if(it == r->end()) it = r->begin(); return *this; }
		iterator operator++(int) { iterator tmp = *this; ++*this; return tmp; }
		iterator& operator--(void) { if(it == r->begin()) it = r->end(); --it; return *this; }
		iterator operator--(int) { iterator tmp = *this; --*this; return tmp; }
		iterator insert(const T& x) { return iterator(*r, r->insert(it, x)); }
		iterator erase(void) { return iterator(*r, r->erase(it)); }
	};
  bool empty(void) {return lst.empty();}
	void clear(void) { lst.clear(); }
	iterator insert(iterator pos, const T& x) { return pos.insert(x); }
	T & operator[](unsigned int n) { return lst[n % lst.size() ]; }
	
	void push_back(const T& x) { lst.push_back(x); }
	iterator begin(void) { return iterator(lst, lst.begin()); }
	int size(void) { return lst.size(); }
};
