#include "Path.hpp"

Path::Path(void) : closed(false), _calculated(false)
{
}

void Path::translate(const translateAttr & t)
{
  for(dxfPointVector::iterator i = _points.begin(); i != _points.end(); i ++)
    i->translate(t);
}

const dxfPointVector & Path::const_points(void)
{
  return _points;
}

dxfPoint Path::toGcode(ostream & out, int & line_num, const translateAttr & translate, bool output_first_point, ostream & plot) const
{
  return this->toGcode(out,line_num,translate,this->_points.begin(),output_first_point,plot);
}
dxfPoint Path::toGcode(ostream & out, int & line_num, const translateAttr & translate, const dxfPointVector::const_iterator & begin_point,bool output_first_point, ostream & plot) const
{
  bool first = true;
  dxfPointVector::const_iterator i;
  for(i = begin_point; first || i != begin_point;)
  {
    if (!first || output_first_point)
		{
			i->toGcode(out,line_num,translate,true);
			plot << "CUT: " << dxfObject::pretty_coord(*i) << endl;
		}

    first = false;
    if (++i == _points.end())
      i = _points.begin();
  }
	if (closed && _points.size() > 1)
	{
    i->toGcode(out,line_num,translate,true);
		plot << "CUT: " << dxfObject::pretty_coord(*begin_point) << endl;
    begin_point->dump("PLOT: ",plot);
	}
	else // Open path -- rewind by 1 point, since the for loop ends with i == begin_point
	{
		if (i == _points.begin())
			i = _points.end()-1;
		else
			i--;
	}

	// cout << "returning: " << dxfObject::pretty_coord(*i) << endl;
  return *i;
}

void Path::dump(const char * prefix, ostream & out)
{
	out << prefix << "Path (" << (closed ? "Closed" : "Open") << "):" << endl;
	string p = prefix;
	p += " ";
	for(dxfPointVector::iterator i = _points.begin(); i != _points.end(); i++)
	{
		i->dump(p.c_str(),out);
	}
}

dxfPointVector & Path::points(void)
{
  _calculated = false;
  return _points;
}

dxfFloat Path::area(void) 
{ 
  _calculate();
  return _area_cached.abs();
}

bool Path::clockwise() 
{ 
  _calculate();
  return _area_cached < 0; 
}

void Path::_calculate(void)
{
  if (_calculated)
    return;

  _area_cached = _area();

}

void Path::reverse(void)
{
  dxfPointVector t = _points;
  _points.clear();
  for(dxfPointVector::reverse_iterator i = t.rbegin(); i != t.rend(); i ++)
    _points.push_back(*i);
}

void Path::push_back(const dxfPoint & p)
{
  _points.push_back(p);
  _calculated = false;
}

dxfPoint & Path::back(void)
{
  return _points.back();
}

dxfPoint & Path::operator[](int i)
{
  return _points[i];
}

size_t Path::size(void)
{
  return _points.size();
}

bool Path::empty(void)
{
  return _points.empty();
}

dxfPointVector::const_iterator Path::findNearestPoint(dxfPoint origin, dxfFloat threshold)
{

	if (!closed)
	{
		dxfFloat dist_begin = dxfObject::distance2D(origin,_points.front());
		dxfFloat dist_end   = dxfObject::distance2D(origin,_points.back());

		if (dist_end < dist_end)
			reverse();

		return _points.begin();
	}

  dxfPointVector::iterator nearest_point = _points.begin();
  dxfFloat                 nearest_point_dist = dxfObject::distance2D(origin,*nearest_point);

  dxfPoint                 nearest_line_point = *nearest_point;
  dxfFloat                 nearest_line_point_dist = nearest_point_dist;


  for(dxfPointVector::iterator i = _points.begin();  i != _points.end(); i++)
  {
    dxfPointVector::iterator x = i;
    if (++x == _points.end())
      x = _points.begin();

    dxfLine  seg(*i,*x);
    dxfPoint p = seg.nearestPoint(origin);
    dxfFloat dist = dxfObject::distance2D(p,origin);

    //cout << dxfObject::pretty_coord(*i) << " " << dxfObject::pretty_coord(*x) << " np: " << dxfObject::pretty_coord(p) << " ("<<dist<<")"<<endl;
    if (dist < nearest_line_point_dist)
    {
      nearest_line_point_dist = dist;
      nearest_line_point = p;
      nearest_point = i;
    }
  }

  dxfPointVector::const_iterator start_point = nearest_point;

  //cout << "           nearest: " << dxfObject::pretty_coord(*nearest_point) << endl;
  //cout << "nearest line point: " << dxfObject::pretty_coord(nearest_line_point) << endl;

  if (threshold >= 0) // < 0 means don't split
  {
    // Determine if we need to split the line segment in two.

    dxfPointVector::iterator next_point = nearest_point;

    if(++next_point == _points.end())
      next_point = _points.begin();

    nearest_point_dist = dxfObject::distance2D(*nearest_point,origin);


    if ((nearest_point_dist - nearest_line_point_dist).abs() > threshold)
      start_point = _points.insert(next_point,nearest_line_point);
  }
  return start_point;
  
}

dxfFloat Path::_area(void)
{
  if (!closed)
    return 0;

  int size = _points.size();

  int i,j;
  dxfFloat area = 0;

  for (i=0;i<size;i++) 
  {
    j = (i + 1) % size;
    area += _points[i].x * _points[j].y;
    area -= _points[i].y * _points[j].x;
  }
  return (area / 2);
}

void Path::findBounds(dxfPoint & lower_left, dxfPoint & upper_right, dxfPoint & left, dxfPoint & right, dxfPoint & top, dxfPoint & bottom, bool reset)
{
  if (reset)
  {
    upper_right.x = upper_right.y = upper_right.z = 
      lower_left.x = lower_left.y = lower_left.z = 0;

    if (!_points.empty())
      upper_right = lower_left = (_points.back());
  }

  for (dxfPointVector::const_iterator i = _points.begin(); i != _points.end(); i++)
  {
    if (i->x < lower_left.x)
    {
      lower_left.x = i->x;
      left = *i;
    }

    if (i->x > upper_right.x)
    {
      upper_right.x = i->x;
      right = *i;
    }

    if ( i->y > upper_right.y)
    {
      upper_right.y = i->y;
      top = *i;
    }

    if (i->y < lower_left.y)
    {
      lower_left.y = i->y;
      bottom = *i;
    }
  }
}
