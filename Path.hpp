

#ifndef PATH_HPP
#define PATH_HPP

#include <vector>
#include "dxf.hpp"

using namespace std;

class Path;

typedef vector < Path > PathVector;

class Path
{
  public:

    Path(void);

    void     translate(const translateAttr & t);

    dxfFloat area(void);
    bool     clockwise();
    void     reverse();

    dxfPointVector::const_iterator findNearestPoint(dxfPoint origin, dxfFloat threshold = -1); // -1 means don't split
    void     findBounds(dxfPoint & lower_left, dxfPoint & upper_right, dxfPoint & left, dxfPoint & right, dxfPoint & top, dxfPoint & bottom, bool reset = true);

    bool closed;

    void       push_back(const dxfPoint & p);
    dxfPoint & back(void);
    dxfPoint & operator[](int i);
    size_t     size(void);
    bool       empty(void);

    dxfPointVector & points(void);
    const dxfPointVector & const_points(void);

    dxfPoint       toGcode(ostream & out, int & line_num, const translateAttr & translate, const dxfPointVector::const_iterator & start_point, bool output_first_point, ostream & plot) const;
    dxfPoint       toGcode(ostream & out, int & line_num, const translateAttr & translate, bool output_first_point,ostream & plot) const;
		void           dump(const char * prefix = "", ostream & out = cout);
  private:

    dxfFloat       _area(void);
    void           _calculate(void);
    dxfPointVector _points;
    bool           _calculated;
    dxfFloat       _area_cached;


};

#endif
