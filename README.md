#CODEG

Command line tool to generate Gcode for CNC applications from DXF files. Originally written for 2-axis machines such as foam cutters, it now supports 3-axis machines.

